-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw/01_test_conv2D - Tester le couche conv2D
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour tester le couche conv2D.

00_test_conv2D_tf.py
 - Logiciel pour tester une couche conv2D du Keras en utilisant TensorFlow,
   et Numpy et Reshape.

01_test_conv2D_tf.py
 - Logiciel pour tester une couche conv2D du Keras en utilisant TensorFlow. et
   un tableau Numpy 4-D.

02_test_conv2D_Numpy.py 
 - Logiciel pour tester une couche conv2D avec TensorDot du Numpy.

03_jaffe_gris_cnn.py 
 - Modèle CNN pour application FER qui utilise seulment la Conv2D du Keras avec 
   TensorFlow.

04_test_h5py.py 
 - Logiciel pour comprendre comme on peut utiliser h5py pour sauve-garder les 
   modèles du CNN pour application FER qui utilise seulment la Conv2D du 
   Keras avec TensorFlow.

05_test_conv2D_fil_tf.py
 - Logiciel pour tester une couche conv2D  avec 2 filtres du Keras en
   utilisant TensorFlow. et un tableau Numpy 4-D.

06_test_conv2D_mnist_tf.py
 - Logiciel pour tester une couche conv2D  avec 12 filtres du Keras en
   utilisant TensorFlow. et un tableau Numpy 4-D. Le valeurs du filtre
   correspondre à un modèle pour Mnist.

07_test_conv2D_mnist_1fil_tf.py
 - Logiciel pour tester une couche conv2D  avec 1 et 2 filtres du Keras en
   utilisant TensorFlow. et un tableau Numpy 4-D. Le valeurs du filtre
   correspondre à un modèle pour Mnist.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
