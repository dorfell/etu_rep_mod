# -*- coding: utf-8 -*-
## 
# @file    07_test_conv2D_mnist_1fil_tf.py 
# @brief   Couche conv2D  et filtres pour Mnist
# @details Module pour étuder les calculs fait en la couche conv2D 
#          avec 1 filtres pour le modèle Mnist. Pris du:
#          https://www.tensorflow.org/api_docs/python/tf/nn/conv2d
#          https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D
#          gitlab.com/dorfell/fer_acc_dev/00_sw/
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/15
# @version 0.1
"""@package docstring
"""

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
 
import matplotlib.pyplot as plt
import numpy      as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys
np.set_printoptions(threshold=sys.maxsize)                 # Printing all the weights
import tensorflow as tf
from   tensorflow import keras


print("************************************** ");
print(" Image d'entrée                        ");
print("************************************** ");
# Définir l'image d'entrée (28,28,1)
img_test = \
[[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   3,  18,  18,  18, 126, 136, 175,  26, 166, 255, 247, 127,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  30,  36,  94, 154, 170, 253, 253, 253, 253, 253, 225, 172, 253, 242, 195,  64,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,  49, 238, 253, 253, 253, 253, 253, 253, 253, 253, 251,  93,  82,  82,  56,  39,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,  18, 219, 253, 253, 253, 253, 253, 198, 182, 247, 241,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  80, 156, 107, 253, 253, 205,  11,   0,  43, 154,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,  14,   1, 154, 253,  90,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 139, 253, 190,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  11, 190, 253,  70,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  35, 241, 225, 160, 108,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  81, 240, 253, 253, 119,  25,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  45, 186, 253, 253, 150,  27,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  16,  93, 252, 253, 187,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 249, 253, 249,  64,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  46, 130, 183, 253, 253, 207,   2,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  39, 148, 229, 253, 253, 253, 250, 182,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  24, 114, 221, 253, 253, 253, 253, 201,  78,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  23,  66, 213, 253, 253, 253, 253, 198,  81,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  18, 171, 219, 253, 253, 253, 253, 195,  80,   9,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,  55, 172, 226, 253, 253, 253, 253, 244, 133,  11,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0, 136, 253, 253, 253, 212, 135, 132,  16,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]];
img_test = np.asarray(img_test);                           # Convertir données à tableau numpy
print(img_test); print(img_test.shape); print( type(img_test) ); 

# Imprimer quelques images
fig = plt.figure(figsize=(3,3));
plt.imshow(img_test, cmap="gray", interpolation=None);
plt.title(r"$~5~$");
plt.tight_layout();
#plt.show();

# Pre-processer l'image d'entrée: ajouter dimension et convertir à virgule flottante
img_test = np.expand_dims( img_test, axis=0 ).astype( np.float32 );
#print(img_test); print(img_test.shape);
#sys.exit(0);                                              # Terminer l'execution

print(" \n ");
print("************************************** ");
print(" Conv2D keras layer                    ");
print("************************************** ");

# Définir filtre
'''filtre = np.array([
 [[[-0.12844177]],
  [[-0.16685875]],
  [[ 0.08510285]]],
   
 [[[ 0.04557722]],
  [[ 0.02045484]],
  [[-0.10674687]]],

 [[[ 0.05906213]],
  [[-0.13949758]],
  [[-0.15389141]]] ]);
biais = np.array([-0.09777036]); '''

filtre = np.array([
 [[[-0.12844177,  0.02697711]],
  [[-0.16685875, -0.02324458]],
  [[ 0.08510285, -0.13900204]]],
  
 [[[ 0.04557722, -0.0478156 ]],
  [[ 0.02045484, -0.07291291]],
  [[-0.10674687,  0.08335776]]],
 
 [[[ 0.05906213,  0.00631778]],
  [[-0.13949758, -0.01854884]],
  [[-0.15389141,  0.01237105]]] ]);
biais = np.array([-0.09777036, -0.03156478]);
params = [filtre, biais];         
#sys.exit(0);                                              # Terminer l'execution

# Créer modèle
model = keras.Sequential([ 
  keras.layers.InputLayer( input_shape=(28, 28) ),  
  keras.layers.Reshape( target_shape=(28, 28, 1) ), 
  keras.layers.Conv2D( filters=2, kernel_size=(3,3), activation="relu" ) ]);

# Sauvegarder les paramètres
model.set_weights(params);

# Confirmer qu'il sont sauvegardées correctement
print("--> Paramètres: \n", model.get_weights());

# Faire la prediction avec l'entrée et le filtre
extractor = keras.Model(inputs=model.input,
                        outputs= [layer.output for layer in model.layers] );
features  = extractor(img_test);
print("--> Features: \n", features);

print("--> Résultats: \n");
pred = model.predict(img_test);
print(pred);
#sys.exit(0);                                              # Terminer l'executioni


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
