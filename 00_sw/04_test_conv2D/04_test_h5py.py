#!/usr/bin/env python

# Écrit le fichier
#----------------------------------
import h5py

# Create random data
import numpy as np

data_matrix = np.random.uniform(-1, 1, size=(10, 3));
print(data_matrix);

# Write data to HDF5
fic_don =  h5py.File('file.hdf5', 'w');
fic_don.create_dataset('group_name', data=data_matrix);


# Lire le fichier
#----------------------------------
import h5py
#filename = 'file.hdf5'
#filename = "06_jaffe_gris_cnn_m2_128x128_aug_Adam_100_epoques_hdf5.hdf5";
filename = "06_jaffe_gris_cnn_m2_128x128_aug_Adam_100_epoques_hdf5_weights.hdf5";

fic = h5py.File(filename, 'r');
print("fic:  ", fic); 
print("name: ", fic.name);
print("attr: ", fic.attrs);

# List all groups
print(list(fic.keys()));
a_group_key = list(fic.keys())[0];

# Get the data
#data = list(fic[a_group_key]);
#print(data);

# Get the data
#data = list(fic["model_weights"]);
#print(data);

#data = list(fic["optimizer_weights"]);
#print(data);

data = list(fic["conv2d_1"]);
print(data);
