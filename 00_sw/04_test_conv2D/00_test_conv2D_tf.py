# -*- coding: utf-8 -*-
## 
# @file    00_test_conv2D_tf.py 
# @brief   Couche conv2D. 
# @details Module pour étuder les calculs fait en la couche conv2D 
#          avec 1 filtre et tableau Numpy. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/15
# @version 0.1
"""@package docstring
"""

from numpy import asarray
import tensorflow as tf
from tensorflow    import keras


print(" \n ");
print("************************************** ");
print(" Conv2D keras layer                    ");
print("************************************** ");

# define input data
data = [[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0]];
data = asarray(data); data = data.reshape(1, 8, 8, 1);
print("--> Entrée: \n", data);

# create model
model = keras.Sequential([ 
  keras.layers.Conv2D( filters=1, kernel_size=(3,3), input_shape=(8, 8, 1) ) ]);

# define a vertical line detector
detector = [[[[0]],[[1]],[[0]]],
            [[[0]],[[1]],[[0]]],
            [[[0]],[[1]],[[0]]]]
weights = [asarray(detector), asarray([0.0])];             # [weights, bias]

# store the weights in the model
model.set_weights(weights);

# confirm they were stored
print("--> Paramètres: \n", model.get_weights());

# apply filter to input data
yhat = model.predict(data)
print("--> Résultats: \n");
for r in range(yhat.shape[1]):
	# print each column in the row
	print([yhat[0,r,c,0] for c in range(yhat.shape[2])])


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
