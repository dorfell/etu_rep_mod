# -*- coding: utf-8 -*-
## 
# @file    
# @brief   FER avec CNNs: JAFFE_GRIS_CNN 
# @details Ce module permettre l'entraînament d'un CNN avec
#          le base de données JAFFE et les images prè-traîtées
#          en échelle des gris. En plus sauvegarder le modèle 
#          en format hdf5.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/03/14
# @version 0.1
"""@package docstring
"""
################################
## 03_jaffe_gris_cnn
## by Dorfell Parra
## 2020/03/14
#################################

import keras
import numpy as np

from keras.models import Sequential
from keras.models import model_from_json
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D
from keras.layers import Dense, Activation, Dropout, Flatten

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

from keras import backend as K
from keras.utils import plot_model

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


# Variables
#------------------------------
# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];
num_classes = 7 #angry, disgust, fear, happy, sad, surprise, neutral
batch_size = 1024
#epochs = 100
epochs = 2

print(batch_size,  'batch_size');
print(epochs,      'epochs');
print(num_classes, 'num_classes');

# Charger données dès fichiers binaires *.npy
#img_dat = np.load("../../db/don_traite_jaffe_dlib_128x128.npy"); 
#img_lab = np.load("../../db/lab_traite_jaffe_dlib_128x128.npy"); 
#img_dat = np.load("../db/don_traite_jaffe_dlib_128x128.npy"); 
#img_lab = np.load("../db/lab_traite_jaffe_dlib_128x128.npy"); 
#img_dat = np.load("../db/don_traite_jaffe_dlib_128x128_4img.npy"); 
#img_lab = np.load("../db/lab_traite_jaffe_dlib_128x128_4img.npy"); 
img_dat = np.load("../db/don_traite_jaffe_dlib_128x128_1img.npy"); 
img_lab = np.load("../db/lab_traite_jaffe_dlib_128x128_1img.npy"); 


#print("Img dat: ", img_dat, img_dat.shape, type(img_dat), img_lab);
print("Img dat: ", img_dat.shape, type(img_dat));
print("Img lab: ", img_lab.shape, type(img_lab));

# Dessiner quelques données
#fig=plt.figure(figsize=(16, 150));
#cols = 2;  rows = 2;
#for i in range(1, cols*rows + 1):
#    img = np.squeeze(img_dat[i]);
#    ax  = fig.add_subplot(rows, cols, i);
#    lab = fer_eti[img_lab[i]];
#    ax.set_xlabel(lab);
#    plt.imshow(img, cmap = 'gray');
#plt.subplots_adjust(top=0.9)
#plt.show();


# Data transformation for train and test sets
#------------------------------
# 10-fold cross validation
# The entire dataset is divided into 10 sets, 9 for training, 1 for testing
x_train = np.array(img_dat[0:190],   "float32");
y_train = np.array(img_lab[0:190],   "float32"); 
y_train = keras.utils.to_categorical(y_train, num_classes); # One-hot encoded

x_test  = np.array(img_dat[190:214], "float32");
y_test  = np.array(img_lab[190:214], "float32"); 
y_test  = keras.utils.to_categorical(y_test, num_classes); # One-hot encoded

x_train /= 255; # normalize inputs between [0, 1]
x_test  /= 255; # normalize inputs between [0, 1]

print(x_train.shape[0], 'train samples');
print(x_test.shape[0],  'test samples' );


# De l'entrée du (128,128), les premières (5,5)
#----------------------------------------
x_train = np.asarray([ [
    [  [ 0.02352941], [ 0.01960784], [ 0.01960784], [ 0.01960784], [ 0.01960784] ],
    [  [ 0.01176471], [ 0.01176471], [ 0.01176471], [ 0.01176471], [ 0.01960784] ],
    [  [ 0.        ], [ 0.01176471], [ 0.01960784], [ 0.01960784], [ 0.01176471] ],
    [  [ 0.01960784], [ 0.01176471], [ 0.01176471], [ 0.01960784], [ 0.01960784] ],
    [  [ 0.01176471], [ 0.01176471], [ 0.01176471], [ 0.01176471], [ 0.01960784] ] ]  ]);
#print(x_train);

# Kernel fixé (5,5)
#----------------------------------------
kernel = np.asarray( [ 
[ [[ 0.25066656]],[[-0.23929037]],[[-0.338937  ]],[[ 0.30439377]],[[-0.25060636]] ],
[ [[-0.05378729]],[[-0.31338415]],[[ 0.08372328]],[[-0.25343987]],[[-0.07343876]] ],
[ [[-0.10473406]],[[ 0.26447827]],[[-0.13266687]],[[ 0.31293648]],[[ 0.25636268]] ],
[ [[ 0.05483827]],[[ 0.20035684]],[[ 0.3280502 ]],[[ 0.17483824]],[[ 0.31104422]] ],
[ [[-0.29192019]],[[ 0.10363734]],[[ 0.14986688]],[[-0.18760525]],[[-0.10535523]] ] 
]);

# weights = kernel + bias
weights = [kernel, np.asarray([0.0])];


# Construct CNN structure
#------------------------------
model = Sequential();

# 1st convolution layer
#model.add(Conv2D(1, (5, 5), padding="same", activation="relu", input_shape=(128,128,1)));
#model.add(Conv2D(1, (5, 5), padding="same", activation="relu", input_shape=(5,5,1)));
model.add(Conv2D(1, (5, 5), padding="valid", activation="relu", input_shape=(5,5,1)));

# store the weights in the model
model.set_weights(weights)

#model.add(MaxPooling2D(pool_size=(2,2), strides=(2, 2))); 

# 2nd convolution layer
#model.add(Conv2D(128, (5, 5), padding="same", activation="relu"));
#model.add(AveragePooling2D(pool_size=(2,2), strides=(2, 2)));

# 3rd convolution layer
#model.add(Conv2D(256, (5, 5), padding="same", activation="relu"));
#model.add(AveragePooling2D(pool_size=(2,2), strides=(2, 2)));
#model.add(Flatten());

# fully connected neural networks
#model.add(Dense(1024, activation='relu'));
#model.add(Dropout(0.2));
#model.add(Dense(512, activation='relu'));
#model.add(Dropout(0.2));
#model.add(Dense(num_classes, activation='softmax'))

model.summary();
plot_model(model, to_file="model.png", show_shapes="True");

# Les poids du modèle 
#------------------------------
import sys
np.set_printoptions(threshold=sys.maxsize);

print(" \n ");
print("Kernel et bias du modèle  ");
print("--------------------------");
print(len(model.get_weights())); print(len(model.get_weights()[0]));
print(len(model.get_weights()[0][0])); print(len(model.get_weights()[0][0][0]));
print(model.get_weights());

print(" \n ");
print("Entrée du modèle          ");
print("--------------------------");
print(len(x_train)); print(len(x_train[0]));
print(len(x_train[0][0])); print(len(x_train[0][0][0]));
print(x_train);

print(" \n ");
print("Prediction du modèle      ");
print("--------------------------");
prd = model.predict(x_train);
print(len(prd)); print(len(prd[0]));
print(len(prd[0][0])); print(len(prd[0][0][0]));
print(prd);




# Batch process, augmentation de données
#------------------------------
#datagen = ImageDataGenerator(); # Sans augmentation
datagen = ImageDataGenerator(
          rotation_range=5,
          #width_shift_range=0.2,
          #height_shift_range=0.2,
          #zoom_range=0.2,
          horizontal_flip=True);


# Training
#------------------------------
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(), metrics=['accuracy']);


#fit = True;
#if fit == True:
#        # history = model.fit_generator(x_train, y_train, epochs=epochs, verbose=2) # train for all trainset
#        history = model.fit_generator(
#                  datagen.flow(x_train, y_train, batch_size=batch_size),
#                  steps_per_epoch = np.ceil(len(x_train) / batch_size), 
#                  epochs=epochs, verbose=2, 
#                  validation_data=(x_test, y_test), workers=4); # train for randomly selected one
#else:
#        model.load_weights('data/facial_expression_model_weights.h5') # load weights



# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)


# En sauvegardant le modèle en hdf5 
#------------------------------
model.save("modèles/06_jaffe_gris_cnn_m2_128x128_aug_Adam_100_epoques_hdf5.hdf5");
model.save_weights("'modèles/06_jaffe_gris_cnn_m2_128x128_aug_Adam_100_epoques_hdf5_weights.hdf5");
print("----------------------------");
print("¡Le modèle a été sauvegardé!");
print("----------------------------");


# Evaluating
#------------------------------
"""
# overall evaluation
score = model.evaluate(x_test, y_test)

"""
#------------------------------
#score = model.evaluate(x_test, y_test, verbose=0);
#print('Test loss:', score[0]);
#print('Test accuracy:', 100*score[1]);

#predictions = model.predict(x_test);
#predicted_label = np.argmax(predictions[0]);

#print(history.history);
#print(history.history.keys());


# Fonction qui sert à mesurer le performance du modèle 
#with PdfPages("entrainement_graphique.pdf")  as export_pdf:
    
    # summarize history for accuracy
#    plt.plot(history.history['accuracy']);
#    plt.plot(history.history['val_accuracy']);
#    plt.title('model accuracy');
#    plt.ylabel('accuracy'); plt.xlabel('epoch');
#    plt.legend(['train', 'test'], loc='upper left');
#    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
#    plt.plot(history.history['loss']);
#    plt.plot(history.history['val_loss']);
#    plt.title('model loss');
#    plt.ylabel('loss'); plt.xlabel('epoch');
#    plt.legend(['train', 'test'], loc='upper left');
#    export_pdf.savefig(); plt.close();




print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
