# -*- coding: utf-8 -*-
## 
# @file    06_test_conv2D_mnist_tf.py 
# @brief   Couche conv2D  et filtres pour Mnist
# @details Module pour étuder les calculs fait en la couche conv2D 
#          avec 12 filtres pour le modèle Mnist. Pris du:
#          https://www.tensorflow.org/api_docs/python/tf/nn/conv2d
#          https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D
#          gitlab.com/dorfell/fer_acc_dev/00_sw/
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/15
# @version 0.1
"""@package docstring
"""

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
 
import matplotlib.pyplot as plt
import numpy      as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys
np.set_printoptions(threshold=sys.maxsize)                 # Printing all the weights
import tensorflow as tf
from   tensorflow import keras


print("************************************** ");
print(" Image d'entrée                        ");
print("************************************** ");
# Définir l'image d'entrée (28,28,1)
img_test = \
[[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   3,  18,  18,  18, 126, 136, 175,  26, 166, 255, 247, 127,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  30,  36,  94, 154, 170, 253, 253, 253, 253, 253, 225, 172, 253, 242, 195,  64,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,  49, 238, 253, 253, 253, 253, 253, 253, 253, 253, 251,  93,  82,  82,  56,  39,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,  18, 219, 253, 253, 253, 253, 253, 198, 182, 247, 241,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  80, 156, 107, 253, 253, 205,  11,   0,  43, 154,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,  14,   1, 154, 253,  90,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 139, 253, 190,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  11, 190, 253,  70,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  35, 241, 225, 160, 108,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  81, 240, 253, 253, 119,  25,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  45, 186, 253, 253, 150,  27,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  16,  93, 252, 253, 187,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 249, 253, 249,  64,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  46, 130, 183, 253, 253, 207,   2,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  39, 148, 229, 253, 253, 253, 250, 182,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  24, 114, 221, 253, 253, 253, 253, 201,  78,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,  23,  66, 213, 253, 253, 253, 253, 198,  81,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  18, 171, 219, 253, 253, 253, 253, 195,  80,   9,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,  55, 172, 226, 253, 253, 253, 253, 244, 133,  11,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0, 136, 253, 253, 253, 212, 135, 132,  16,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]];
img_test = np.asarray(img_test);                           # Convertir données à tableau numpy
print(img_test); print(img_test.shape); print( type(img_test) ); 

# Imprimer quelques images
fig = plt.figure(figsize=(3,3));
plt.imshow(img_test, cmap="gray", interpolation=None);
plt.title(r"$~5~$");
plt.tight_layout();
#plt.show();

# Pre-processer l'image d'entrée: ajouter dimension et convertir à virgule flottante
img_test = np.expand_dims( img_test, axis=0 ).astype( np.float32 );
#print(img_test); print(img_test.shape);
#sys.exit(0);                                              # Terminer l'execution

print(" \n ");
print("************************************** ");
print(" Conv2D keras layer                    ");
print("************************************** ");

# Définir filtre
filtre = np.array([
 [[[-0.12844177,  0.02697711, -0.13031466,  0.10899138,  0.07460558, -0.08195725,  0.03221923,  0.0346565 , -0.08686271, -0.21204038, -0.18540785, -0.00663945]],
  [[-0.16685875, -0.02324458,  0.08018055, -0.14959002, -0.03603126, -0.2098718 , -0.13181174, -0.16482992, -0.13009673, -0.06438071, -0.15049484,  0.01006977]],
  [[ 0.08510285, -0.13900204, -0.03523267, -0.10833992,  0.0797712 , -0.13602825, -0.07490212, -0.22563894, -0.2303134 , -0.13538213, -0.0786192 ,  0.0103737 ]]],
   
 [[[ 0.04557722, -0.0478156 , -0.06276748, -0.05370886, -0.10526456,  0.14173804, -0.00126711, -0.12402703,  0.06558444, -0.05340183,  0.03801798,  0.05407466]],
  [[ 0.02045484, -0.07291291,  0.05922719, -0.16991857, -0.1636278 , -0.00317965, -0.02638013,  0.08150954,  0.08718065,  0.04693826,  0.0279058 , -0.01948467]],
  [[-0.10674687,  0.08335776,  0.02575829,  0.13348253, -0.12968771, -0.02561011,  0.03615002, -0.07648547, -0.08838228, -0.15456153, -0.09756929, -0.00332136]]],

 [[[ 0.05906213,  0.00631778,  0.09479371, -0.00630973, -0.01226371, -0.08016151,  0.04961815, -0.11242632, -0.00382802,  0.06425937,  0.05016313, -0.10772046]],
  [[-0.13949758, -0.01854884, -0.16699773, -0.12907258,  0.04704671, -0.16654006, -0.16358167,  0.10788805,  0.06633822, -0.15949596,  0.03542057,  0.07783928]],
  [[-0.15389141,  0.01237105, -0.12985401, -0.00539328,  0.01192932,  0.04931073,  0.06715702,  0.04802407, -0.04510075,  0.01511548, -0.11922237,  0.03790081]]] ]);

biais = np.array([-0.09777036, -0.03156478, -0.11302432,  0.0364575 , -0.1042959 , -0.03120471, -0.23655404, -0.11773806, -0.0608803 , -0.02927797, -0.01248365, -0.15345518]);
params = [filtre, biais];         
#sys.exit(0);                                              # Terminer l'execution

# Créer modèle
model = keras.Sequential([ 
  keras.layers.InputLayer( input_shape=(28, 28) ),  
  keras.layers.Reshape( target_shape=(28, 28, 1) ), 
  keras.layers.Conv2D( filters=12, kernel_size=(3,3), activation="relu" ) ]);

# Sauvegarder les paramètres
model.set_weights(params);

# Confirmer qu'il sont sauvegardées correctement
print("--> Paramètres: \n", model.get_weights());

# Faire la prediction avec l'entrée et le filtre
extractor = keras.Model(inputs=model.input,
                        outputs= [layer.output for layer in model.layers] );
features  = extractor(img_test);
print("--> Features: \n", features);

print("--> Résultats: \n");
pred = model.predict(img_test);
print(pred);

#sys.exit(0);                                              # Terminer l'executioni


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
