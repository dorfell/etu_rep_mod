# -*- coding: utf-8 -*-
## 
# @file    05_test_conv2D_fil_tf.py 
# @brief   Couche conv2D et filtres 
# @details Module pour étuder les calculs fait en la couche conv2D 
#          avec 2 filtres. Pris du:
#          https://www.tensorflow.org/api_docs/python/tf/nn/conv2d
#          https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/15
# @version 0.1
"""@package docstring
"""

import numpy      as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys
import tensorflow as tf
from   tensorflow import keras


print(" \n ");
print("************************************** ");
print(" Conv2D keras layer                    ");
print("************************************** ");

# Définir entrée
ent =  np.array([[
   [[2], [1], [0]],
   [[1], [2], [2]],
   [[1], [1], [0]] ]]);
print("--> Entrée: \n", ent);

# Définir filtre
filtre = np.array([
 [ [[2, 1]], [[0, 0]] ],
 [ [[0, 0]], [[1, 1]] ]  ]);
print("--> Filtre: \n", filtre);
params = [np.asarray(filtre), np.asarray([0.0, 0.0])];          # [weights, bias]

# Créer modèle
model = keras.Sequential([ 
  keras.layers.Conv2D( filters=2, kernel_size=(2,2), input_shape=(3, 3, 1) ) ]);

# Sauvegarder les paramètres
model.set_weights(params);

# Confirmer qu'il sont sauvegardées correctement
print("--> Paramètres: \n", model.get_weights());

# Faire la prediction avec l'entrée et le filtre
pred = model.predict(ent);
print("--> Résultats: \n");
print(pred);
for r in range(pred.shape[1]):
	# print each column in the row
	print([pred[0,r,c,0] for c in range(pred.shape[2])])

#sys.exit(0);                                              # Terminer l'executioni


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
