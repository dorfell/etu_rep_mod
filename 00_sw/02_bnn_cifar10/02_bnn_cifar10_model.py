# -*- coding: utf-8 -*-
## 
# @file    02_bnn_cifar10_model.py 
# @brief   Modèle binaire BNN pour Cifar10. 
# @details Module pour faire l'entraînement d'un BNN  avec 
#          l'ensemble des données CIFAR10, qui utilise les
#          couchess binaires reproduites en Keras.
#          Pris du: 
#          https://github.com/itayhubara/BinaryNet.tf
#          https://github.com/uranusx86/BinaryNet-on-tensorflow
#          https://github.com/DingKe/nn_playground/tree/master/binarynet
#          https://github.com/Haosam/Binary-Neural-Network-Keras
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/28
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{mathrsfs}'];
mpl.rcParams.update({'font.size': 12});

import matplotlib.pyplot as plt 
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights
import tempfile
import tensorflow as tf
from   tensorflow import keras
from mes_fonctions import *

 
# Charger le base de données CIFAR10
cifar10 = keras.datasets.cifar10;
(train_images, train_labels), (test_images, test_labels) = cifar10.load_data();
#train_images = train_images[0:5000]; train_labels = train_labels[0:5000];
#test_images  = test_images[0:5000];  test_labels  = test_labels[0:5000];

#train_images = train_images[0:10]; train_labels = train_labels[0:10];
#test_images  = test_images[0:10];  test_labels  = test_labels[0:10];

cifar10_classes = \
[r"$~Airplane~$", r"$~Automobile~$", r"$~Bird~$",  r"$~Cat~$",  r"$~Deer~$", 
 r"$~Dog~$",      r"$~Frog~$",       r"$~Horse~$", r"$~Ship~$", r"$~Truck~$"];
#print(test_images[0]); print(test_images[0].shape); print( type(test_images[0]) ); 
#sys.exit(0);                                              # Terminer l'execution

# Imprimer quelques images
fig = plt.figure(figsize=(3,3));
plt.imshow(train_images[0], cmap="gray", interpolation=None);
plt.title( cifar10_classes[ int(train_labels[0]) ] );
plt.tight_layout();
#plt.show();
#sys.exit(0);                                              # Terminer l'execution

# Normalize the input image so that each pixel value is between 0 to 1
#train_images = train_images / 255.0;
#test_images  = test_images  / 255.0;

# (-H,+H) les limites de valeurs
H = 1.0;

# Paramètres du BatchNormalization
alpha = 0.1; epsilon = 1.0e-4;


print(" \n ");
print("************************************** ");
print(" Modèle                                ");
print("************************************** ");
# Définir l'architecture du modèle
model = keras.Sequential( [
  keras.layers.InputLayer( input_shape=(32, 32, 3) ),
  keras.layers.Reshape( target_shape=(32, 32, 3) ),
 
  Conv2D_BinaryLayer( filters=128, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),

  Conv2D_BinaryLayer( filters=128, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),
  
  Conv2D_BinaryLayer( filters=256, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),

  Conv2D_BinaryLayer( filters=256, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),

  Conv2D_BinaryLayer( filters=512, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),

  Conv2D_BinaryLayer( filters=512, kernel_size=(3, 3), strides=[1,1,1,1], padding="SAME", H=H, use_bias=True),
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),

  keras.layers.Flatten( ),
  Dense_BinaryLayer( 1024, H=H, use_bias=True ), 
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),
  
  Dense_BinaryLayer( 1024, H=H, use_bias=True ), 
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon),
  Tanh_BinaryLayer(),

  Dense_BinaryLayer( 10, H=H, use_bias=True ),
  keras.layers.BatchNormalization(momentum=1.0-alpha, epsilon=epsilon)  ]);
model.summary();
 
# initiate Adam optimizer
#epochs = 1;
epochs = 120; 
lr_start = 1e-3; lr_end = 1e-7;
lr_decay = (lr_end / lr_start)**(1./epochs);
lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(lr_start, 
                                                             decay_steps=1000, 
                                                             decay_rate=lr_decay,
                                                             staircase=True);
opt = keras.optimizers.Adam(learning_rate=lr_schedule);

# Train the digit classification model
model.compile( optimizer=opt,
               loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
               metrics=["accuracy"] ); 

model.fit( train_images, train_labels, batch_size=None, epochs=epochs, validation_split=0.3 );

# Evaluer le modèle
_, model_accuracy = model.evaluate( test_images, test_labels, verbose=0 );
print("Exactitude du modèle: ", model_accuracy);

# En sauvegardant le modèle en H5 
#------------------------------
model.save("models/bnn_cifar10.h5");                       # Sauvegarder toût
#model.save_weights("models/bnn_cifar10.h5");


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
