# -*- coding: utf-8 -*-
## 
# @file    mes_fonctions.py 
# @brief   Module des fonctions nécessaires  
# @details Module qui contient les fonctions pour lire les modèles crée
#          avec TensorFlow en format hdf5 et h5.
#          Pris du:
#          https://keras.io/guides/making_new_layers_and_models_via_subclassing/
#          https://github.com/keras-team/keras/blob/2.2.4/keras/layers/convolutional.py
#          https://github.com/uranusx86/BinaryNet-on-tensorflow/blob/master/binary_layer.py
#          https://github.com/MatthieuCourbariaux/BinaryNet/blob/master/Train-time/binary_net.py
#          https://medium.com/analytics-vidhya/tf-gradienttape-explained-for-keras-users-cc3f06276f22
#          https://stackoverflow.com/questions/55552715/tensorflow-2-0-minimize-a-simple-function
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/11/15
# @version 0.1
"""@package docstring
"""

import numpy as np
#np.set_printoptions(threshold=sys.maxsize) # Printing all the weights

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3';                  # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys

import tensorflow as tf
from tensorflow import keras


#--------------------------------------
# Fonctions pour la binarization
#--------------------------------------
def hard_sigmoid(x):
  return tf.clip_by_value((x + 1.)/2., 0, 1);

def round_through(x):
  '''Element-wise rounding to the closest integer with full gradient propagation.
     A trick from [Sergey Ioffe](http://stackoverflow.com/a/36480182)
     a op that behave as f(x) in forward mode,
     but as g(x) in the backward mode.      ?????           '''
  rounded = tf.math.round(x);                              # Rounds au plus proche entier
  return (x + tf.stop_gradient(rounded-x));                # Arrêt le calcul du gradient.

def binary_tanh_unit(x):
  ''' The neurons' activations binarization function
      # It behaves like the sign function during forward propagation
      # And like:
      #   hard_tanh(x) = 2*hard_sigmoid(x)-1
      # during back propagation ????????                   '''
  return 2.0*round_through(hard_sigmoid(x))-1.0;

def binarization(W, H):
  ''' Fonction pour binarizer le paramètre poids (W).
      Pris du dépôt BinaryNet de uranusx86 en Github.    '''
  Wb = tf.math.round( hard_sigmoid(W / H) );
  return tf.cast( tf.where(Wb, -H, H), tf.float32);


#--------------------------------------
# Définition des couches binaires
#--------------------------------------
class Tanh_BinaryLayer(tf.keras.layers.Layer):
  ''' Cette class fait la définition d'une couche
      Tanh binaire. Pris du binary_tanh_unit().            '''
  def __init__(self, **kwargs):
    super(Tanh_BinaryLayer, self).__init__(**kwargs);
 
  def call(self, inputs):
    return binary_tanh_unit(inputs);


class Conv2D(tf.keras.layers.Layer):
  ''' Cette class reproduire une couche Convolutional.     '''
  def __init__(self, filters=32, kernel_size=(3,3), strides=[1,1,1,1], padding="VALID", use_bias=False, **kwargs):
    super(Conv2D, self).__init__(**kwargs);
    self.filters  = filters;  self.kernel_size = kernel_size;
    self.strides  = strides;  self.padding     = padding;
    self.use_bias = use_bias;
 
  def build(self, input_shape):
    # NHWC format --> input_shape[-1] == "channels_last"
    input_dim    = input_shape[-1];   
    kernel_shape = tuple(self.kernel_size) + (input_dim, self.filters);
    self.kernel = self.add_weight( shape       = kernel_shape,
                                   initializer = tf.keras.initializers.GlorotUniform,
                                   name        = "kernel",
                                   regularizer = None,
                                   constraint  = None,
                                   trainable   = True);
    if self.use_bias:
      self.bias = self.add_weight( shape       = (self.filters, ),
                                   initializer = tf.keras.initializers.Zeros,
                                   name        = "bias",
                                   regularizer = None,
                                   constraint  = None,
                                   trainable   = True);
    else: 
      self.bias = None;
  
  def call(self, inputs):
    outputs = tf.nn.conv2d(inputs, self.kernel, strides=self.strides, padding=self.padding);
    if self.use_bias:
      outputs = tf.nn.bias_add(outputs, self.bias);
    return outputs;

  def get_config(self):                           
    ''' Méthode necessaire pour sauvergardé le modèle. '''
    config = super().get_config().copy();
    config.update({
      "filters": self.filters, "kernel_size": self.kernel_size, "strides": self.strides,
      "padding": self.padding, "H": self.H, "use_bias": self.use_bias });
    return config;


class Conv2D_BinaryLayer(tf.keras.layers.Layer):
  ''' Cette class fait la définition d'une couche de
      convolution binaire.                                 '''
  def __init__(self, filters=32, kernel_size=(3,3), strides=[1,1,1,1], padding="VALID", H=1.0, use_bias=False, **kwargs):
    super(Conv2D_BinaryLayer, self).__init__(**kwargs);
    self.filters  = filters;  self.kernel_size = kernel_size;
    self.strides  = strides;  self.padding     = padding;
    self.H        = H;        self.use_bias    = use_bias;
 
  def build(self, input_shape):
    # NHWC format --> input_shape[-1] == "channels_last"
    input_dim    = input_shape[-1];
    kernel_shape = tuple(self.kernel_size) + (input_dim, self.filters);
    self.kernel  = self.add_weight(shape       = kernel_shape,
                                   initializer = tf.keras.initializers.RandomUniform(-self.H, self.H),
                                   name        = "kernel",
                                   regularizer = None,
                                   constraint  = lambda w: tf.math.round( tf.clip_by_value(w, -self.H, self.H) ),
                                   trainable   = True);

    if self.use_bias:
      self.bias = self.add_weight( shape       = (self.filters, ),
                                   initializer = tf.keras.initializers.Zeros,
                                   name        = "bias",
                                   regularizer = None,
                                   constraint  = lambda w: tf.math.round( tf.clip_by_value(w, -self.H, self.H) ),
                                   trainable   = True);
    else:
      self.bias = None;

    self.k_bin = 0;                                        # Kernels binaires
  
  def call(self, inputs):
    #self.k_bin = binarization(self.kernel, self.H);        # Binarization du kernel
    self.k_bin = self.kernel;                               # Effect en add_weights constraints.
    outputs    = tf.nn.conv2d(inputs, self.k_bin, strides=self.strides, padding=self.padding);
    if self.use_bias:
      outputs = tf.nn.bias_add(outputs, self.bias);
    return outputs;

  def get_config(self):                           
    ''' Méthode necessaire pour sauvergardé le modèle. '''
    config = super().get_config().copy();
    config.update({
      "filters": self.filters, "kernel_size": self.kernel_size, "strides": self.strides,
      "padding": self.padding, "H": self.H, "use_bias": self.use_bias });
    return config;


class Dense(tf.keras.layers.Layer):
  ''' Cette class reproduire une couche full connected.    '''
  def __init__(self, units=32, use_bias=False, **kwargs):
    super(Dense, self).__init__(**kwargs);
    self.units = units;
    self.use_bias  = use_bias;

  def build(self, input_shape):
    self.w = self.add_weight( shape       = (input_shape[-1], self.units),
                              # NHWC format --> input_shape[-1] == "channels_last"
                              initializer = tf.keras.initializers.GlorotUniform,
                              trainable   = True);
    if self.use_bias:
      self.bias = self.add_weight( shape       = (self.units, ),
                                   initializer = tf.keras.initializers.Zeros,
                                   trainable   = True);
    else: 
      self.bias = None;
  
  def call(self, inputs):
    outputs = tf.matmul(inputs, self.w);
    if self.use_bias:
      outputs = tf.nn.bias_add(outputs, self.bias);
    return outputs;

  def get_config(self):                           
    ''' Méthode necessaire pour sauvergardé le modèle. '''
    config = super().get_config().copy();
    config.update({
      "units": self.units, "use_bias": self.use_bias });
    return config;


class Dense_BinaryLayer(tf.keras.layers.Layer):
  ''' Cette class fait la définition d'une couche
      full connected binaire.                              '''
  def __init__(self, units=32, H=1.0, use_bias=False, **kwargs):
    super(Dense_BinaryLayer, self).__init__(**kwargs);
    self.units    = units;    self.H = H;
    self.use_bias = use_bias;
 
  def build(self, input_shape):      
    # NHWC format --> input_shape[-1] == "channels_last"
    input_dim    = input_shape[-1];
    self.w = self.add_weight( shape       = (input_dim, self.units),
                              initializer = tf.keras.initializers.RandomUniform(-self.H, self.H),
                              name        = "kernel",
                              regularizer = None,
                              constraint  = lambda w: tf.math.round( tf.clip_by_value(w, -self.H, self.H) ),
                              trainable   = True);
    if self.use_bias:
      self.bias = self.add_weight( shape       = (self.units, ),
                                   initializer = tf.keras.initializers.Zeros,
                                   name        = "bias",
                                   regularizer = None,
                                   constraint  = lambda w: tf.math.round( tf.clip_by_value(w, -self.H, self.H) ),
                                   trainable   = True);
    else:
      self.bias = None;

    self.w_bin = 0;                                        # Weights binaires

  def call(self, inputs):
    #self.w_bin = binarization(self.w, self.H);             # Binarization du weights
    self.w_bin = self.w;                                    # Binarization du weights en constraint du add_weigths.
    outputs = tf.matmul(inputs, self.w_bin);
    if self.use_bias:
      outputs = tf.nn.bias_add(outputs, self.bias);
    return outputs;

  def get_config(self):                           
    ''' Méthode necessaire pour sauvergardé le modèle. '''
    config = super().get_config().copy();
    config.update({
      "units": self.units,  "H": self.H, "use_bias": self.use_bias });
    return config;


class HardTanh(tf.keras.layers.Layer):
  ''' Cette class fait la définition d'une couche
      Tanh avec ses valeur limitées dans
      l'intervalle [-1, 1].                                '''
  def __init__(self):
    super(HardTanh, self).__init__();
 
  def call(self, inputs):
    return tf.clip_by_value(inputs, -1, 1);


class ReLU(tf.keras.layers.Layer):
  ''' Cette class fait la reproduction d'une couche
      d'activation ReLU.                                   '''
  def __init__(self):
    super(ReLU, self).__init__();
 
  def call(self, inputs):
    return tf.nn.relu(inputs);



#--------------------------------------
# Couches qui pouvent utilisées comme
# reference pour l'inference.
#--------------------------------------
def conv_fp(entree, filtre, bias, verbose):
  ''' Cette fonction fait la convolution (i.e. matmul, bias,
      ReLU6) utilisée en point flotant en  utilisant Numpy. 
      entree:            tableau Numpy de entrée.
      filtre:            filtre 3x3.
      bias:              valeur bias.
      conv_tab:          tableau avec la convolution.      '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  rangs, colonnes = entree.shape;
  conv_tab = np.zeros( (rangs-2, colonnes-2) );            # Map des caractéristiques
  for i in range(0, rangs - 2):
    for j in range(0, colonnes - 2):

      # Faire la convolution
      #----------------------------------
      ent = entree[0+i:3+i, 0+j:3+j];                      # Desplacer l'entrée
      conv_tab[i][j] = np.tensordot(ent, filtre) + bias;   # W*x + bias
      #print(conv_tab[i][j]);
      conv_tab[i][j] = max(conv_tab[i][j], 0);             # ReLU

  return conv_tab;


def dict_par_conv(parametres, verbose):
  ''' Cette fonction sert à créer un dictionnaire avec 
      les paramètres d'une couche de convolution en 
      format du dictionnaire des listes. La fonction 
      termine avec l'impression des paramètres.
      parametres:   nombre et position du modèle hdf5.              
      verbose:   imprimir les paramètres: 0 min, 1 tout.   '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  par = np.squeeze( parametres );
  rang, col, fil = par.shape;                              # rangs, colonnes et filtres
  filtres = {};                                            # dictionnaires des filtres
  for k in range(0, fil):                                  # initialiser dictionnaire
    filtres[ "fil"+str(k) ] = [];

  # Remplir le dictionnaire
  #----------------------------------
  for i in range(0, rang): 
    for j in range(0, col): 
      #print( " vecteur: \n ", par[i][j] );
      for k in range(0, fil):
        filtres["fil"+str(k)].append( par[i][j][k] );
  print( filtres );
  print(" ");


def dict_par_dense(parametres, verbose):
  ''' Cette fonction sert à créer un dictionnaire avec 
      les paramètres d'une couche full connected en 
      format du dictionnaire des listes. La fonction 
      termine avec l'impression des paramètres.
      parametres:   nombre et position du modèle hdf5.              
      verbose:   imprimir les paramètres: 0 min, 1 tout.   '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  par = np.squeeze( parametres );
  rangxcol, neu = par.shape;                               # rangs x colonnes et neurones
  neurones = {};                                           # dictionnaires des neurones
  for k in range(0, neu):                                  # initialiser dictionnaire
    neurones[ "neu"+str(k) ] = [];

  # Remplir le dictionnaire
  #----------------------------------
  for ij in range(0, rangxcol): 
    #print( " vecteur: \n ", par[ij] );
    for k in range(0, neu):
      neurones["neu"+str(k)].append( par[ij][k] );
  #print( neurones );

  # Imprimer les neurones de la couche
  #----------------------------------
  for k in range(0, neu):
    print("neu"+str(k)+":");
    print( neurones["neu"+str(k)] );
  print(" ");


def flatten_np(entree, verbose):
  ''' Cette fonction fait l'operation  flatten pour convertir
      un tableau à un vecteur en utilisant Numpy.E.g.: un 
      tableau (13, 13) à un vecteur (1, 169).
      entree:   tableau Numpy de entrée (int8).                
      flat_vec: vecteur Numpy de sortie.                   '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  rangs, colonnes = entree.shape;
  flat_vec = [];                                           # Vecteur de sortie
  for i in range(0, rangs):
    for j in range(0, colonnes):

      # Flatten (1, rangs*colonnes)
      #----------------------------------
      flat_vec.append(entree[i, j]);                       # Ajouter éléments au vec.
  flat_vec = np.asarray(flat_vec);
  return flat_vec;      


def full_np(entree, poids, bias, verbose):
  ''' Cette fonction calcule la couche full-connected 
      utilisée par Tensorflowlite en 
      utilisant Numpy.
      entree:            tableau Numpy de entrée (int8).                
      poids:             tableau de poids (int8).                
      bias:              tableau de bias  (int32).                
      output_multiplier: facteur de échelle M0.
      output_shift:      facteur n < 0 de 2^(-n).
      offset_ent:        offset d'entrée.
      offset_sor:        offset de sortie.                
      offset_fil:        offset de filtre.
      full_vec:          vecteur Numpy de sortie.          '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  full_vec = [];                                           # Vecteur de sortiee

  for idx in range(0, len(poids)):

    # Full-connected
    #----------------------------------
    Wx_b = np.tensordot( entree, poids[idx] ) + bias[idx];    # W*x + b
    #print(Wx_b);
    full_vec.append(Wx_b);

  return full_vec;  


def maxpool_fp( entree, verbose):
  ''' Cette fonction fait l'operation  MaxPool avec un 
      fenêtre de (2,2) utilisée  pour tflite en 
      utilisant Numpy. 
      entree:  tableau Numpy de entrée (int8).                
      max_tab: tableau Numpy de sortie.                    '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  rangs, colonnes = entree.shape;
  rangs = int(rangs/2); colonnes = int(colonnes/2);        # Dimensions de sortie
  max_tab = np.zeros( (rangs, colonnes) );                 # Map des caractéristiques
  for i in range(0, rangs):
    for j in range(0, colonnes):

      # MaxPooling (2, 2)
      #----------------------------------
      max_tab[i,j] = np.max(entree[2*i:2*i+2, 2*j:2*j+2]); # Max du fenêtre (2, 2)

  return max_tab;      


#print("                                      ");
#print("**************************************");
#print("*  ¡Merci d'utiliser ce logiciel!    *");
#print("*             (8-)                   *");
#print("**************************************");
