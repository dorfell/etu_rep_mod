-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw /02_bnn_cifar10 - Étude du modèle BNN 
     en TF avec Keras
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour l'étude des calculs fait en un modèle BNN pour Cifar10 et reproduit 
en Numpy avec point flottant.
1
00_cnn_cifar10_fp_model.py
 - Logiciel pour entraîner un modèle CNN avec Cifar10 en Tensorflow  et sauvegardé 
   modèle en format *.h5.

01_ref_cifar10_model.py 
 - Logiciel pour entraîner un modèle CNN avec Cifar10 en utilisant couches qui 
   reproduisant las couches ReLU, Conv et Dense du Keras avec Tensorflow. Le
   modèle est sauvegardé en format *.h5.

02_bnn_cifar10_model.py 
 - Logiciel pour entraîner un modèle BNN avec Cifar10 en Tensorflow  et sauvegardé 
   modèle en format *.h5.

03_bnn_cifar10_result.py 
 - Logiciel pour imprimer les paràmetres du chaque couche du modèle.

mes_fonctions.py
 - Module avec les fonctions utilisées dans ce entredepôt.

modèles/
 - Dossier avec modèles en formats HDF5/h5.
   bnn_cifar10.h5:   modèle sauvegardé avec Keras.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
