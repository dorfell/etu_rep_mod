# -*- coding: utf-8 -*-
## 
# @file    mes_fonctions.py
# @brief   Mes fonctions
# @details Ce module contient les fonctions nécessaires pour exécuter quelques 
#          codes dans ce dépôt.
# @date    2020/06/12
# @version 0.1
"""@package docstring
"""
################################
## Mes fonctions
## by Dorfell Parra 
## 2020/06/12
#################################
from bitarray import bitarray
import numpy as np


def decimal_converter(nom):
  ''' Fonction pour faire la division de la partie flottant.  
      nom: Nombre à diviser. '''
  while nom > 1:
      nom /= 10
  return nom

# Décimale positive à binaire
def decp_bin(nom, Q_int, Q_flt):
  ''' Fonction pour faire la conversion du nombre décimale positive à
      binaire.  
      nom: Nombre à convertir.
      Q_int: Nombre de bits pour la parte entier.
      Q_flt: Nombre de bits pour la parte flottant. '''

  sig = "0";            # bit du signe positive.
  nom_int, nom_dec = nom.split(".");
  nom_int = bin(int(nom_int)).lstrip("0b") + "."; 
  while (len(nom_int) < Q_int):
    nom_int = "0" + nom_int;
  nom = sig + nom_int;  # En ajoutant le bit du signe
  #print(nom_int);

  # Iterate the number of times= the number of decimal places to be 
  nom_dec = int(nom_dec);
  for i in range(Q_flt): 
    # Multiply the decimal value by 2 and seperate the whole number part 
    # and decimal part 
    try: 
      whole, dec = str((decimal_converter(nom_dec)) * 2).split(".") 
    except:
      print("¡Svp, diminuer les nombre de bits pour la part flottant!");
      print("Q_flt suggéré: ", i);
      #print(nom);
      break
    #print("float part", whole, dec);
    # Convert the decimal part to integer again 
    nom_dec = int(dec);   
    nom += whole;
  print("Le binaire est: ", nom);
  return(nom);                


# Décimale negative à binaire
def decn_bin(nom, Q_int, Q_flt):
  ''' Fonction pour faire la conversion du nombre décimale positive à
      binaire.  
      nom: Nombre à convertir.
      Q_int: Nombre de bits pour la parte entier.
      Q_flt: Nombre de bits pour la parte flottant. '''

  sig = "1";            # bit du signe negative.
  nom_int, nom_dec = nom.split(".");
  nom_int = bin(int(nom_int)).lstrip("0b") + "."; 
  while (len(nom_int) < Q_int):
    nom_int = "0" + nom_int;
  nom = sig + nom_int;  # En ajoutant le bit du signe
  #print(nom_int);

  # Iterate the number of times= the number of decimal places to be 
  nom_dec = int(nom_dec);
  for i in range(Q_flt): 
    # Multiply the decimal value by 2 and seperate the whole number part 
    # and decimal part 
    try: 
      whole, dec = str((decimal_converter(nom_dec)) * 2).split(".") 
    except:
      print("¡Svp, diminuer les nombre de bits pour la part flottant!");
      print("Q_flt suggéré: ", i);
      #print(nom);
      break
    #print("float part", whole, dec);
    # Convert the decimal part to integer again 
    nom_dec = int(dec);   
    nom += whole;

  #print(nom);
  whole, dec = nom.split(".") 
  while (len(dec) < Q_flt):
    dec = dec + "0";
  #print(whole, dec);
  nom = whole+dec;                                   # Nombre sans point
  nom_tmp = bitarray(nom);
  nom_tmp = ~(nom_tmp);                              # Complement à 1
  nom_tmp = nom_tmp.to01();                          # À str du 1 et 0
  nom_tmp = bin( int(nom_tmp, 2) + int("0b1",2) );   # Complement à 2
  nom_tmp = sig + nom_tmp[2:];                       # Effacer 0b et ajouter signe
  print("Binaire sans point: ", nom_tmp);
  nom =  nom_tmp[0:Q_int] + "." + nom_tmp[Q_int:Q_int + Q_flt];
  print("Le binaire est: ", nom);
  return(nom);                


# Binaire à décimale
def bin_dec(nom, Q_int, Q_flt):
  ''' Fonction pour faire la conversion du nombre binaire à décimale.  
      nom: Nombre à convertir.
      Q_int: Nombre de bits pour la parte entier.
      Q_flt: Nombre de bits pour la parte flottant. '''
  sig = nom[0]; 
  if (sig == "0"):  # nombre positive
    nom_int = nom[1:Q_int]; nom_flt = nom[Q_int:Q_int + Q_flt];
    nom_int = int(nom_int, 2); 
    nom_flt = int(nom_flt, 2)/(2**len(nom_flt));
    print(nom_int + nom_flt);
  else:             # nombre negative
    nom_tmp = bitarray(nom);
    nom_tmp = ~(nom_tmp);                              # Complement à 1
    nom_tmp = nom_tmp.to01();                          # À str du 1 et 0
    nom_tmp = bin( int(nom_tmp, 2) + int("0b1",2) );   # Complement à 2
    nom_tmp = nom_tmp[2:];                             # Effacer 0b
    while ( len(nom_tmp) < (Q_int + Q_flt) ):          # Garder zeros à gauche
      nom_tmp = "0" + nom_tmp;
    nom_int = nom_tmp[1:Q_int]; nom_flt = nom_tmp[Q_int:Q_int + Q_flt];
    nom_int = int(nom_int, 2); 
    nom_flt = int(nom_flt, 2)/(2**len(nom_flt));
    nom_dec = (-1)*(nom_int + nom_flt);
    print("Décimale: ", nom_dec);
