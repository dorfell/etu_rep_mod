#https://solarianprogrammer.com/2019/07/18/python-using-c-cpp-libraries-ctypes/
""" Python wrapper for the C shared library mylib"""
import sys, platform
from ctypes import *
 
#my_lib = cdll.LoadLibrary("mes_fon_C/mylib.so");
#my_lib = CDLL("mes_fon_C/mylib.so");  # Bibliothèque partagée de C
my_lib = CDLL("mes_fon_CPP/mylib.so"); # Bibliothèque partagée de CPP
print(my_lib);

print("\n--> Try test_empty: ");
my_lib.test_empty();

print("\n--> Try test_add_int: ");
print( "Addition avec nombres entiers: ", my_lib.test_add_int(2, 3) );

print("\n--> Try test_add_float: ");
my_lib.test_add_float.restype = c_float;
print( "Addition avec nombres flotantes: ", my_lib.test_add_float( c_float(1.987654321), c_float(3.098090909) ) );

print("\n--> Try test_cpp_frexp: ");
my_lib.test_cpp_frexp.restype = c_double;
M = c_double(0.000004207);
#exp = POINTER(c_int);
exp = c_int();
q = my_lib.test_cpp_frexp( M , byref(exp) );
#q = my_lib.test_cpp_frexp( M , pointer(exp)  );

#pointer(c_int).my_lib(test_cpp_frexp, "exp");
print(" --- Résultats en Python ---");
print( "M=   ", M .value);
print( "q=   ", q );
print( "exp= ", exp );
print( "exp= ", exp.value );
