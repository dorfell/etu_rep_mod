#include <stdio.h>
#include "../include/mylib.h"
 
void test_empty(void) {  puts("Hello from C");  }
 
int test_add_int(int x, int y) {
  printf("Resultat %d: \n", (x+y) );
  return x + y;  }
 
float test_add_float(float x, float y) {
  printf("Resultat %f: \n", (x+y) );
  return x + y;  }

void test_passing_array(int *data, int len) {
  printf("Data as received from Python\n");
  for(int i = 0; i < len; ++i) {
    printf("%d ", data[i]); }
  puts("");
 
  // Modifying the array
  for(int i = 0; i < len; ++i) {
    data[i] = -i;  }
}
