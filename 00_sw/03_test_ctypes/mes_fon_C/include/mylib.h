#pragma once
 
#ifdef __cplusplus
extern "C" {
#endif
 
void  test_empty(void);
int   test_add_int(int x, int y);
float test_add_float(float x, float y);
void  test_passing_array(int *data, int len);

#ifdef __cplusplus
}
#endif
