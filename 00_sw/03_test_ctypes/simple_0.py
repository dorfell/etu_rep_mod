# https://solarianprogrammer.com/2019/07/18/python-using-c-cpp-libraries-ctypes/
"""Simple example of loading and using the system C library from Python"""

import sys, platform
import ctypes, ctypes.util

# Get the path to the system C library
#-------------------------------------
if platform.system() == "Windows":
  path_libc = ctypes.util.find_library("msvcrt");
else:
  path_libc = ctypes.util.find_library("c");

# Get a handle to the sytem C library
#-------------------------------------
try:
  libc = ctypes.CDLL(path_libc);
except OSError:
  print("Unable to load the system C library");
  sys.exit();
 
print(f'Succesfully loaded the system C library from "{path_libc}"');

# Preuve 1: Imprimer dès fonction C en Python
#-------------------------------------
libc.puts(b"Hello from Python to C");
libc.printf(b"%s\n", b"Using the C printf function from Python ... ");

# Preuve 2: Travailer avec mutables strings.
#-------------------------------------
mut_str = ctypes.create_string_buffer(10);                 # Create a mutable string
libc.memset(mut_str, ctypes.c_char(b"X"), 5);              # Fill the first 5 elements with 'X':
libc.puts(mut_str);                                        # Print the modified string
# Add 4 'O' to the string starting from position 6
p = ctypes.cast(ctypes.addressof(mut_str) + 5, ctypes.POINTER(ctypes.c_char));
libc.memset(p, ctypes.c_char(b"0"), 4);                    # Fill 4 slots with 0
libc.puts(mut_str);                                        # Print the modified string




