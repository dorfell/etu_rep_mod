#include <stdio.h>
#include <iostream>
#include <cmath>
#include "../include/mylib.h"
 
void test_empty(void) {  
  std::cout << "Hello from CPP" << std::endl;  }
 
int test_add_int(int x, int y) {
  std::cout << "Resultat: " << (x+y) << std::endl;
  return x + y;  }
 
float test_add_float(float x, float y) {
  std::cout << "Resultat: " << (x+y) << std::endl;
  return x + y;  }

double test_cpp_frexp(double double_multiplier, int* exp){
  double q; 
  std::cout << "preuve std::frexp" << std::endl;
  q = std::frexp(double_multiplier, exp);
  std::cout << "--- Résultats en CPP ---" << std::endl;
  std::cout << "M= "   << double_multiplier << std::endl; 
  std::cout << "q= "   << q    << std::endl; 
  std::cout << "exp= " << *exp << std::endl; 
  return q;
}
