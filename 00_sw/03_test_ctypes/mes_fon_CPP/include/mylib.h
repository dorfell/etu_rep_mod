#pragma once
 
#ifdef __cplusplus
extern "C" {
#endif
 
void  test_empty(void);
int   test_add_int(int x, int y);
float test_add_float(float x, float y);
double test_cpp_frexp(double double_multiplier, int* exp);
 
#ifdef __cplusplus
}
#endif
