# -*- coding: utf-8 -*-
## 
# @file    00_cnn_cifar10_fp_model.py 
# @brief   CNN  avec CIFAR10. 
# @details Module pour faire l'entraînement d'un CNN  avec 
#          l'ensemble des données CIFAR10.
#          Pris du: 
#          keras/examples/cifar10_cnn.py
#          ''' 
#          Train a simple deep CNN on the CIFAR10 small images dataset.
#          It gets to 75% validation accuracy in 25 epochs, and 79% after 50 epochs.
#          (it's still underfitting at that point, though).
#          '''
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/10/28
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{mathrsfs}'];
mpl.rcParams.update({'font.size': 12});

import matplotlib.pyplot as plt 
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights
import tempfile
import tensorflow as tf
from   tensorflow import keras
#from mes_fonctions import *

 
# Charger le base de données CIFAR10
cifar10 = keras.datasets.cifar10;
(train_images, train_labels), (test_images, test_labels) = cifar10.load_data();
test_images = test_images[0:1000];
test_labels = test_labels[0:1000];
cifar10_classes = \
[r"$~Airplane~$", r"$~Automobile~$", r"$~Bird~$",  r"$~Cat~$",  r"$~Deer~$", 
 r"$~Dog~$",      r"$~Frog~$",       r"$~Horse~$", r"$~Ship~$", r"$~Truck~$"];
#print(test_images[0]); print(test_images[0].shape); print( type(test_images[0]) ); 
#sys.exit(0);                                              # Terminer l'execution

# Imprimer quelques images
fig = plt.figure(figsize=(3,3));
plt.imshow(train_images[0], cmap="gray", interpolation=None);
plt.title( cifar10_classes[ int(train_labels[0]) ] );
plt.tight_layout();
#plt.show();
#sys.exit(0);                                              # Terminer l'execution

# Normalize the input image so that each pixel value is between 0 to 1
train_images = train_images / 255.0;
test_images  = test_images  / 255.0;

print(" \n ");
print("************************************** ");
print(" Modèle                                ");
print("************************************** ");
# Définir l'architecture du modèle
model = keras.Sequential( [
  keras.layers.InputLayer( input_shape=(32, 32, 3) ),
  keras.layers.Reshape( target_shape=(32, 32, 3) ),
 
  keras.layers.Conv2D( filters=32, kernel_size=(3, 3), activation="relu", padding="same" ),
  keras.layers.Conv2D( filters=32, kernel_size=(3, 3), activation="relu", padding="same" ),
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),
  keras.layers.Dropout( 0.25 ),
  
  keras.layers.Conv2D( filters=64, kernel_size=(3, 3), activation="relu", padding="same" ),
  keras.layers.Conv2D( filters=64, kernel_size=(3, 3), activation="relu", padding="same" ),
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),
  keras.layers.Dropout( 0.25 ),
  
  keras.layers.Flatten( ),
  keras.layers.Dense( 512, activation="relu" ),
  keras.layers.Dropout( 0.25 ),
  keras.layers.Dense( 10, activation="softmax" ) ]);
 
# initiate RMSprop optimizer
opt = keras.optimizers.RMSprop(learning_rate=0.0001, decay=1e-6);
 
# Train the digit classification model
model.compile( optimizer=opt,
               loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
               metrics=["accuracy"] ) ;
model.summary();

#model.fit( train_images, train_labels, epochs=1, validation_split=0.1 );
model.fit( train_images, train_labels, epochs=50, validation_split=0.1 );

# Evaluer le modèle
_, model_accuracy = model.evaluate( test_images, test_labels, verbose=0 );
print("Exactitude du modèle: ", model_accuracy);

# En sauvegardant le modèle en H5 
#------------------------------
#model.save("models/cnn_cifar10_fp.h5");                   # Sauvegarder toût
#model.save_weights("models/model_weights.h5");


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
