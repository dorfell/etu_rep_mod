-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw/05_pretraitement_dlib_jaffe/ - Pretraîtement d'ensamble
     des données JAFFE
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour l'étude du pretraîtement d'ensamble de données JAFFE.

00_lbp_comparaison/
 - Logiciel pour faire le pretraîtement des images et la comparaison des méthodes de Motif Locaux Binaire (Local Binary Pattern LBP) avec  les méthodes Uniform, NRI-Uniform, ROR et VAR. 

01_var_canny_comparaison/
 - Logiciel pour faire le pretraîtement des images, implementer le motif locaux binaire (LBP) avec le  méthode var,  et la comparaison des valeurs minVal, maxVal du filtre Canny 1986 pour la détection de contours.

02_var_canny_redim_comparaison/
 - Logiciel pour faire le pretraîtement des images, implementer le motif locaux binaire (LBP) avec le  méthode var,  et comparer le redimensionnement des images de 128 x 128 pixels à 24 x 24 px, 32 x 32 px, 48 x 48 px, 56 x 56 px, 72 x 72 px, 96 x 96 px.

03_jaffe_lbp_canny_redim/
 - Logiciel pour faire le pretraîtement des images, implementer le motif locaux binaire (LBP) avec le  méthode var,  et redimensionner les images  de 128 x 128 pixels à 56 x 56 pixels.

mes_fonctions.py
 - Module avec les fonctions utilisées dans ce entredepôt.

modèles  
 - Dossier avec modèles en format HDF5.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
