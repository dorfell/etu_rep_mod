# -*- coding: utf-8 -*-
## 
# @file    00_jaffe_lbp_canny_redim.py
# @brief   Jaffe avec pretraîtement + lbp + canny + redimensionnement
# @details Ce module sauvegardé l'ensemble Jaffe avec pretraîtement, le
#          motif locaux binaire avec le méthode VAR, la détection de contours
#          avec Canny 1986 et les seuils: minVal = 50, maxVal = 100, et le
#          redimensionnement de 128 x 128 pixels à 56 x 56 pixels avec 
#          l'interpolation Lanczos4.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/01
# @version 0.1
"""@package docstring
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
loc_db = "../../db/jaffedbase/";                           # Seulment 4 images. 
#loc_db = "../../db/jaffedbase_orig/";                     # 213 images. 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pretraîtement des données             ");
print("************************************** ");
# Pre-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pretraîtement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
plt.imshow(img_dat_pre[0], cmap="gray"); 
plt.title("Image pretraîté");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

plt.imshow(lbp_var, cmap="gray");                          # Imprimer quelques images
plt.title("Image avec lbp, méthode= var"); plt.show();
print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
var_int_lt = [];
for img in lbp_var_lt:
  img = img / img.max();                                   # normalizer img [0, 1].
  img = 255 * img;                                         # À échelle 0 - 255
  img = np.rint(img);                                      # Arrondir à entier
  var_int_lt.append( img.astype(np.uint8) );               # cast à uint8
print("valeurs du lbp_var_int[0]:\n ", var_int_lt[0]); print(var_int_lt[0].shape);
#print("valeurs du lbp_var_int:\n ", var_int_lt); 
print("longitude de liste: ", len(var_int_lt));

plt.imshow(var_int_lt[0], cmap="gray");                    # Imprimer quelques images
plt.title("Image avec lbp var en entier 0 à 255");  plt.show();
#sys.exit(0);                                             # Terminer l'execution


print("************************************** ");
print(" Détection des contours                ");
print("************************************** ");
# minVal = 50, maxVal = 100
can_lt = [];
for img in var_int_lt:
  img = cv2.Canny(img, 50, 100); 
  can_lt.append(img);

plt.imshow(can_lt[0], cmap="gray");                        # Imprimer quelques images
plt.title("Image avec détection de contours"); plt.show();
print("Image après détection de contours: \n", can_lt[0]); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
red_lt = [];
for img in can_lt:
  # 56 x 56 pixels
  lan_56 = cv2.resize(img, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
  red_lt.append(lan_56);

plt.imshow(red_lt[0], cmap="gray");                        # Imprimer quelques images
plt.title("Interpolation Lanczos4 56 x 56 pixels"); plt.show();
print("Image après le redimensionnement: \n", red_lt[0]); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Sauvegarder et charger les images     ");
print("************************************** ");
# Sauver à fichier binaire de Numpy *.npy
np.save("../../db/jaffe_pre_var_can_red56x56_img.npy", red_lt );
np.save("../../db/jaffe_pre_var_can_red56x56_lab.npy", img_lab);

# Charger le fichier binaire *.npy
img_lt = np.load("../../db/jaffe_pre_var_can_red56x56_img.npy");
lab_lt = np.load("../../db/jaffe_pre_var_can_red56x56_lab.npy");

# Imprimer quelques images et ses étiquettes
for idx, img in enumerate(img_lt):
  print("Valeurs des image " + fer_eti[ lab_lt[idx] ] + ": \n", img, img.dtype); 
  plt.imshow(img, cmap="gray"); 
  plt.title(fer_eti[ lab_lt[idx] ]); plt.show();


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
