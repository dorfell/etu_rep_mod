-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw /00_fp_mnist-Étude des modèles du Tensorflow
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour l'étude de l'inference des modèles avec Tensorflow en point flottant.

00_cnn_mnist_fp_model.py 
 - Logiciel pour faire l'entraînement d'un CNN avec l'ensamble des données MNIST 
   avec Tensorflow..

01_cnn_mnist_fp_result.py 
 - Logiciel pour faire l'impression des tensors et ses entrées et sorties avec 
   les fonctions du module mes_fonctions.py. 

02_cnn_mnist_fp_np.py 
 - Logiciel pour reproduire l'inference d'un modèle CNN avec Numpy, en utilisant 
   point flottant. 

etude.log 
 - Résultats du logiciel 01_cnn_mnist_fp_result.py avec le modèle: 
   models/cnn_1fil_mnist_fp.h5.

etude_cnn_12fil.log 
 - Résultats du logiciel 01_cnn_mnist_fp_result.py avec le modèle: 
   models/cnn_12fil_mnist_fp.h5.

etude_cnn_12fil_listes.log 
 - Résultats du logiciel 01_cnn_mnist_fp_result.py avec le modèle: 
   models/cnn_12fil_mnist_fp.h5.

etude_cnn_1filtre.log 
 - Résultats du logiciel 01_cnn_mnist_fp_result.py avec le modèle: 
   models/cnn_1fil_mnist_fp.h5.

mes_fonctions.py 
 - Module avec les fonctions necesaires pour lire les modèles en format hdf5/h5.
   Le fonctions sont appeles pour 01_cnn_mnist_fp_result.py, 02_cnn_mnist_fp_np.py.
   et ils impriment les paramètres.

modèles/
 - Dossier avec modèles en formats HDF5/h5.
   cnn_1fil_mnist_fp.h5:   modèle sauvegardé avec 1  filtres Keras.
   cnn_12fil_mnist_fp.h5:  modèle sauvegardé avec 12 filtres Keras.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
