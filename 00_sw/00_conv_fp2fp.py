# -*- coding: utf-8 -*-
## 
# @file    00_conv_fp.py 
# @brief   convertiseeur entre décimal et point fixé. 
# @details Logiciel pour convertir entre decimal et représentation du point fixé.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/06/03
# @version 0.1
"""@package docstring
"""
################################
## 00_conv_fp
## by Dorfell Parra 
## 2020/06/03
#################################
import numpy as np
from bitarray import bitarray

from mes_fonctions import *

def decimal_converter(num):  
    while num > 1: 
        num /= 10
    return num 

print("**************************************");
print("* Logiciel de conversion entre       *");
print("* décimale et binaire                *");
print("**************************************");
print("                                      ");
print("  Svp, choisir la conversion:         ");
print("  --> 0: Décimale à binaire.          ");
print("  --> 1: Binaire à décimale.          ");

cho = int(input());

if (cho == 0):
  print("                                      ");
  print("Conversion à binaire");
  print("**************************************");
  nom = input("Entrer le nombre décimale: ");
  Q = input("Entrer le format Q. Ex. Si Q3,13 alors [3,13]:");
  Q_int, Q_flt = Q.split(",");
  Q_int = int(Q_int); Q_flt = int(Q_flt);
  if ( float(nom) < 0 ): # décimal negative
    decn_bin(nom, Q_int, Q_flt);
  else:                  # décimal positive.
    decp_bin(nom, Q_int, Q_flt);

elif (cho == 1):
  print("                                      ");
  print("Conversion à décimale");
  print("**************************************");
  nom = str(input("Entrer le nombre binaire: "));
  Q = input("Entrer le format Q. Ex. Si Q3,13 alors 3,13:");
  Q_int, Q_flt = Q.split(",");
  Q_int = int(Q_int); Q_flt = int(Q_flt);
  bin_dec(nom, Q_int, Q_flt);

else:
  print("Mauvais choix 8-(.");


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
