# -*- coding: utf-8 -*-
## 
# @file    01_lire_modeles.py 
# @brief   Lire modèles en format hdf5 
# @details Logiciel pour imprimier les modèles sauvegardées comment fichiers hdf5. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2020/06/25
# @version 0.1
"""@package docstring
"""

import numpy
#numpy.set_printoptions(threshold=sys.maxsize) # Printing all the weights

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys

import tensorflow as tf
from tensorflow import keras

from mes_fonctions import *



# Imprimer le modèle
#----------------------------------
print(" ");
print(" Imprimer le modèle");
print(" ===================================== ");
fic_mod   = "models/model.h5";                             # fichier avec le modèle
verbose = 0;                                               # 1 pour imprimer tout
lir_h5_mod(fic_mod, verbose); 
print(" ------------------------------------- ");


# Imprimer le modèle conscient de quantization
#----------------------------------
#print(" ");
#print(" Imprimer le modèle conscient de quantization");
#print(" ===================================== ");
#fic_mod   = "models/q_aware_model.h5";                    # fichier avec le modèle
#q_aware_bin = open(fic_mod, 'rb').read();
#print("quantization aware model: ", q_aware_bin);
#q_aware_bin_w = open(fic_mod_w, 'rb').read();
#print("quantization aware model weights: ", q_aware_bin_w);
# ATTENTION: Il imprime le binaraire, pour une meilleure
# répresentation utilisez NETRON.
#print(" ------------------------------------- ");


# Imprimer le modèle quantifié (tflite)
#----------------------------------
print(" ");
print(" Imprimer le modèle quantifié (tflite)");
print(" ===================================== ");
fic_mod = "models/quantized_tflite_model.tflite";          # fichier avec le modèle
verbose = 0;                                               # 1 pour imprimer tout
lir_tflite_mod(fic_mod, verbose); 
print(" ------------------------------------- ");


# Imprimer le modèle (tflite)
#----------------------------------
print(" ");
print(" Imprimer le modèle (tflite)");
print(" ===================================== ");
fic_mod   = "models/float_tflite_model.tflite";            # fichier avec le modèle
verbose = 0;                                               # 1 pour imprimer tout
lir_tflite_mod(fic_mod,verbose); 
print(" ------------------------------------- ");


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
