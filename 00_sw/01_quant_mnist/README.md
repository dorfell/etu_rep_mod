-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw /02_quant_mnist- Étude du modèles avec quantization
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour l'étude de la quantification des modèles avec TFlite.

00_quan_conv_ker_tf_mnist.py 
 - Logiciel pris du "Quantization aware training in Keras example", pour preuver la
   quantization, le TensorFlow Lite.

01_lire_modeles.py 
 - Logiciel pour faire l'impression des paramètres du chaque modèle avec les fonctions 
   du module mes_fonctions.py. 

02_int_conv_ker_tf_mnist.py 
 - Logiciel pour faire l'entraînement du modèle tflite à tester (e.g. en utilisant 
   1 filtre en la couche  conv2D en lieu du 12) Nom du modèle:
   quantized_tflite_model_test.tflite.

03_etude_modele_tflite.py
 - Logiciel pour imprimer les tensors et ses entrées et sorties. Il sert à  entendre 
   comment ce fait l'inferance.

04_etude_modele_cnn_1filtre_np.py
 - Logiciel pour calculer l'inference avec Numpy. Le modèle correspond à une réseau
   CNN avec 1 filtre (modèle crée en 02_int_conv_ker_tf_mnist.py et nommé 
   quantized_tflite_model_test.tflite). Il sert à entendre  comment ce  fait 
   l'inferance en Numpy.

05_a_etude_modele_cnn_12filtre_np.py
 - Logiciel pour calculer l'inference avec Numpy. Le modèle correspond à une réseau
   CNN avec 12 filtres (modèle crée en 00_quan_conv_ker_tf_mnist.py et nommé
   quantized_tflite_model.tflite). Il sert à  entendre  comment ce  fait 
   l'inferance avec Numpy.

05_b_etude_modele_cnn_12filtre_np.py
 - Logiciel pour calculer l'inference avec Numpy. Le modèle correspond à une réseau
   CNN avec 12 filtres (modèle crée en 00_quan_conv_ker_tf_mnist.py et nommé
   quantized_tflite_model.tflite). Il sert à  entendre  comment ce  fait 
   l'inferance avec Numpy. Dans ce logiciel le résultats de la convolution
   est change de rangées à colonnes, et la couche de Max Pooling seulment
   prennent les premières 7 tableaus de 13x12, où le 12 répresente le nombres de filtres.

06_a_evaluer_modele_cnn_12filtre_np.py
 - Logiciel pour evaluer le perfomance d'un modèle CNN avec 12 filtres en utilisant
   Numpy. Evaluation fait avec les test_images de Mnist. Pour evaluer 05_a.

06_b_evaluer_modele_cnn_12filtre_np.py
 - Logiciel pour evaluer le perfomance d'un modèle CNN avec 12 filtres en utilisant
   Numpy. Evaluation fait avec les test_images de Mnist. Pour evaluer 06_b.

etude_modele_cnn_1filtre_tflite.log 
 - Résultats du logiciel 03_etude_modele_tflite.py avec le modèle: 
   models/quantized_tflite_model_test.tflite.

etude_modele_cnn_12filtres_tflite.log 
 - Résultats du logiciel 03_etude_modele_tflite.py avec le modèle: 
   models/quantized_tflite_model.tflite.

etude_modele_cnn_12filtres_fc_parameters.log 
 - Tensors de la couche full connected du modèle de tflite: 
   models/quantized_tflite_model.tflite.

mes_fonctions.py 
 - Module avec les fonctions necesaires pour lire les modèles en format hdf5/h5, en 
   binaire et en tensorflow lite (tflite). Le fonctions sont appeles pour
   01_lire_modeles.py et ils impriment les paramètres.

modèles/
 - Dossier avec modèles en formats HDF5/h5, binaires et tflite.
   model.h5: modèle sauvegardé avec Keras.
   q_aware_model.h5: modèle conscient de la quantization pour lire comme binaire.
   quantized_tflite_model.tflite: modèle quantifié à nombres entiers en format tflite.
   float_tflite_model.tflite: modèle quantifié à nombres flottants en format tflite.

   Les suivantes modèles ont été crées avec 02_int_conv_ker_tf_mnist.py 
   model_test.h5: modèle sauvegardé avec Keras.
   q_aware_model_test.h5: modèle conscient de la quantization pour lire comme binaire.
   quantized_tflite_model_test.tflite: modèle quantifié à nombres entiers en 
   format tflite. CNN avec 1 filtre.

res/  
 - Dossier avec les résultats des operations en chaque tensor.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
