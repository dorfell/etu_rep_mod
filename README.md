-------------------------------------------------------------------

     =========================================================
     fer_acc_dev - Développement d'un accelerateur pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Ce dépôt a des fichiers  qui sont utilisées pour le développement d'un 
accelerateur FER.

00_sw
 -Développement en software de l'accelerateur.

01_hw  
 - Développement en hardware de l'accelerateur.

docs  
 - Documentation utile pour le développement.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
