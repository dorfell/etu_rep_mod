----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2020 21:57:30 
-- Design Name: 
-- Module Name: div_in - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity div_int is
  port ( in1, in2  :  in integer range -127 to  128;
         div       : out integer range -127 to  128 );
end div_int;

architecture Behavioral of div_int is

begin

-- Faire la division
div <=  in1 / in2; 

end Behavioral;
