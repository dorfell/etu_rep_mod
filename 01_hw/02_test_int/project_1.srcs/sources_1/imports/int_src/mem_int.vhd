----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2020 22:02:57 
-- Design Name: 
-- Module Name: mem_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mem_int is
  port ( sel_in1  :  in std_logic;
         sel_in2  :  in std_logic; 
         A        : out integer range -127 to  128;
         B        : out integer range -127 to  128 );
end mem_int;

architecture Behavioral of mem_int is

signal sig_A_pos, sig_A_neg : integer range -127 to  128;
signal sig_B_pos, sig_B_neg : integer range -127 to  128;

begin

sig_A_pos <=   5;
sig_A_neg <=  -5;

sig_B_pos <=  10;
sig_B_neg <= -10;

A <= sig_A_pos when (sel_in1 = '0') else sig_A_neg;
B <= sig_B_pos when (sel_in2 = '0') else sig_B_neg;

end Behavioral;
