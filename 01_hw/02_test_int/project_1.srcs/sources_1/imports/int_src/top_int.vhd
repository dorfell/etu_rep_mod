----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2020 04:28:30 PM
-- Design Name: 
-- Module Name: top_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top_int is
  port ( sel_in1  :   in std_logic;
         sel_in2  :   in std_logic;
         sel_op   :   in std_logic_vector(1 downto 0); 
         out_int   : out integer range -127 to 128  );
end top_int;

architecture Behavioral of top_int is

  -- Memory Component Declaration
  component mem_int
    port( sel_in1  :  in std_logic;
          sel_in2  :  in std_logic; 
          A        : out integer range -127 to  128;
          B        : out integer range -127 to  128 );
  end component;
  
    -- Fixed Point addition Component
  component add_int
    port( in1, in2  :  in integer range -127 to  128;
          sum       : out integer range -127 to  128 );
  end component;

  -- Fixed Point multiplication Component
  component mul_int
    port( in1, in2  :  in integer range -127 to  128;
          mul       : out integer range -127 to  128 );
  end component;
   
 -- Fixed Point division Component
 component div_int
    port( in1, in2  :  in integer range -127 to  128;
          div       : out integer range -127 to  128 );
  end component;
              
  signal sig_in1, sig_in2 : integer range -127 to  128;
  signal sig_sum, sig_mul : integer range -127 to  128;
  signal sig_div          : integer range -127 to  128;
    
begin

  ins_mem: mem_int port map(
    sel_in1 => sel_in1,
    sel_in2 => sel_in2,
    A       => sig_in1,
    B       => sig_in2 ); 

  ins_add: add_int port map(
    in1 => sig_in1,
    in2 => sig_in2,
    sum => sig_sum ); 

  ins_mul: mul_int port map(
    in1 => sig_in1,
    in2 => sig_in2,
    mul => sig_mul ); 

  ins_div: div_int port map(
    in1 => sig_in1,
    in2 => sig_in2,
    div => sig_div ); 

  -- Sélection de sortie
  out_int <= sig_sum when (sel_op = "00") else
             sig_mul when (sel_op = "01") else
             sig_div when (sel_op = "10") else
             sig_sum;
  
end Behavioral;