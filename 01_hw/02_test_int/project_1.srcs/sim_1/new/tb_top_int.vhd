----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2020 22:51:35 
-- Design Name: 
-- Module Name: tb_top_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_top_int is
--  Port ( );
end tb_top_int;

architecture Behavioral of tb_top_int is

  -- Component Declaration for the Unit Under Test (UUT)
  component top_int is
    port( 
         sel_in1  :  in std_logic;
         sel_in2  :  in std_logic;
         sel_op   :  in std_logic_vector(1 downto 0); 
         out_int  : out integer range -127 to 128  );
  end component;

  -- Inputs
  signal sel_in1 : std_logic;
  signal sel_in2 : std_logic;
  signal sel_op  : std_logic_vector(1 downto 0);

  -- Outputs
  signal out_int :  integer range -127 to 128;

begin

    uut: top_int port map(
         sel_in1 => sel_in1,
         sel_in2 => sel_in2,
         sel_op  => sel_op,
         out_int => out_int
         ); 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 5 ns;	
      --wait;
      
      --  5 et 10   
      sel_in1 <= '0';  sel_in2 <= '0'; 
      sel_op  <= "00"; -- addition/soustraction
      wait for 5 ns;  
      sel_op  <= "01"; -- multiplication
      wait for 5 ns;
      sel_op  <= "10"; -- division
      wait for 5 ns;
      
      --  5 et -10   
      sel_in1 <= '0';  sel_in2 <= '1'; 
      sel_op  <= "00"; -- addition/soustraction
      wait for 5 ns;  
      sel_op  <= "01"; -- multiplication
      wait for 5 ns;
      sel_op  <= "10"; -- division
      wait for 5 ns;

      --  -5 et 10   
      sel_in1 <= '1';  sel_in2 <= '0'; 
      sel_op  <= "00"; -- addition/soustraction
      wait for 5 ns;  
      sel_op  <= "01"; -- multiplication
      wait for 5 ns;
      sel_op  <= "10"; -- division
      wait for 5 ns;

      --  -5 et -10   
      sel_in1 <= '1';  sel_in2 <= '1'; 
      sel_op  <= "00"; -- addition/soustraction
      wait for 5 ns;  
      sel_op  <= "01"; -- multiplication
      wait for 5 ns;
      sel_op  <= "10"; -- division
      wait for 5 ns;
      wait; 

      
end process;		

end Behavioral;