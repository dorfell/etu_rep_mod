----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/23/2020 03:46:35 PM
-- Design Name: 
-- Module Name: tb_test_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;


entity tb_test_fp is
--  Port ( );
end tb_test_fp;

architecture Behavioral of tb_test_fp is

  -- Component Declaration for the Unit Under Test (UUT)
  component test_fp
      port( sum, sub : out sfixed(5 downto -3);
            mul      : out sfixed(9 downto -6);
            div      : out sfixed(9 downto -6) );
--            res : out sfixed(4 downto -3)  ); 
  end component;
  
  --Outputs
  signal sig_sum, sig_sub  : sfixed(5 downto -3);
  signal sig_mul           : sfixed(9 downto -6); 
  signal sig_div           : sfixed(9 downto -6);
--  signal sig_res           : sfixed(9 downto -6);

begin

    uut: test_fp port map(
         sum => sig_sum,
         sub => sig_sub,
         mul => sig_mul
--         div => sig_div,
--         res => sig_res
         ); 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 5 ns;	
      --wait;
end process;		

end Behavioral;
