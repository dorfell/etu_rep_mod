----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2020 11:31:30 AM
-- Design Name: 
-- Module Name: mul_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;


entity div_fp is
  port ( in1, in2  :  in sfixed(4 downto -3);
         div       : out sfixed(9 downto -6) );
end div_fp;

architecture Behavioral of div_fp is

signal sig_num1, sig_num2  : sfixed(4 downto -3);
signal sig_div             : sfixed(9 downto -6);


begin

sig_num1 <= in1; -- Entrée 1
sig_num2 <= in2; -- Entrée 2

div <= sig_div;  -- Sortie

-- Faire la division
sig_div <=  sig_num1 / sig_num2;    -- div


end Behavioral;
