----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/24/2020 04:07:27 PM
-- Design Name: 
-- Module Name: add_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;


entity add_fp is
  port ( in1, in2  :  in sfixed(4 downto -3);
         sum       : out sfixed(5 downto -3) );
end add_fp;


architecture Behavioral of add_fp is

signal sig_num1, sig_num2  : sfixed(4 downto -3);
signal sig_sum             : sfixed(5 downto -3);


begin

sig_num1 <= in1; -- Entrée 1
sig_num2 <= in2; -- Entrée 2

sum <= sig_sum;  -- Sortie

-- Faire l'addition
sig_sum <=  sig_num1 + sig_num2;    -- sum = "001100010" = 12.25


end Behavioral;
