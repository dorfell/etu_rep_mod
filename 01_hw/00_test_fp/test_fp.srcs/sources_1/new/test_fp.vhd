----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/23/2020 12:57:30 PM
-- Design Name: 
-- Module Name: test_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--library ieee_proposed;
--use ieee_proposed.fixed_pkg.all;
use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;


entity test_fp is
  Port ( --n1, n2 : in  ufixed(4 downto -3);
         sum, sub : out sfixed(5 downto -3);
         mul      : out sfixed(9 downto -6);
         div      : out sfixed(9 downto -6) );
--         res : out sfixed(4 downto -3) ); 
end test_fp;

architecture Behavioral of test_fp is

signal sig_n1, sig_n2    : sfixed(4 downto -3);
signal sig_sum, sig_sub  : sfixed(5 downto -3);
signal sig_mul           : sfixed(9 downto -6); 
signal sig_div           : sfixed(9 downto -6);
--signal sig_res           : sfixed(4 downto -3);


begin

sig_n1  <=  to_sfixed (5.75, sig_n1);     -- n1  =  "00101110" = 5.75
sig_n2  <=  to_sfixed (6.5,  sig_n2);     -- n2  =  "00110100" = 6.5
sig_sum <=  sig_n1 + sig_n2;              -- sum = "001100010" = 12.25
--See that the range of 'n3' is 5 downto -3 but that of 'n1' and 'n2' is 4 downto -3.

sig_sub <=  sig_n2 - sig_n1;              -- sub =        "000000110" = 0.75
sig_mul <=  sig_n1 * sig_n2;              -- mul = "0000100101011000" = 37.375
sig_div <=  sig_n2 / sig_n1;              -- div = "0000000001001001" = 1.1304
--sig_res <=  sig_n2 rem sig_n1;            -- sub = "0000000000000110" = 0.75

--sig_n1 <= n1;
--sig_n2 <= n2;

sum <= sig_sum;
sub <= sig_sub;
mul <= sig_mul;
div <= sig_div;
--res <= sig_res;

end Behavioral;
