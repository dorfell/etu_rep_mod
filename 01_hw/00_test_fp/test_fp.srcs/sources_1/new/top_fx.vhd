----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/24/2020 04:28:30 PM
-- Design Name: 
-- Module Name: top_fx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;

entity top_fx is
  port ( sel_in1  :  in std_logic;
         sel_in2  :  in std_logic;
         sel_op   :  in std_logic_vector(1 downto 0); 
         out_fx   : out sfixed(9 downto -6) );
end top_fx;

architecture Behavioral of top_fx is

  -- Memory Component Declaration
  component mem_fp
    port( sel_in1  :  in std_logic;
          sel_in2  :  in std_logic; 
          A        : out sfixed(4 downto -3);
          B        : out sfixed(4 downto -3) );
  end component;
  
    -- Fixed Point addition Component
  component add_fp
    port( in1, in2  :  in sfixed(4 downto -3);
          sum       : out sfixed(5 downto -3) );
  end component;

  -- Fixed Point multiplication Component
  component mul_fp
    port( in1, in2  :  in sfixed(4 downto -3);
          mul       : out sfixed(9 downto -6) );
  end component;
  
  -- Fixed Point division Component
  component div_fp
    port( in1, in2  :  in sfixed(4 downto -3);
          div       : out sfixed(9 downto -6) );
  end component;
  
            
  signal sig_in1, sig_in2 : sfixed(4 downto -3);
  signal sig_sum          : sfixed(5 downto -3);
  signal sig_mul, sig_div : sfixed(9 downto -6);
    
begin

  ins_mem: mem_fp port map(
    sel_in1 => sel_in1,
    sel_in2 => sel_in2,
    A       => sig_in1,
    B       => sig_in2 ); 


  ins_add: add_fp port map(
    in1 => sig_in1,
    in2 => sig_in2,
    sum => sig_sum  ); 

  ins_mul: mul_fp port map(
    in1 => sig_in1,
    in2 => sig_in2,
    mul => sig_mul  ); 

  ins_div: div_fp port map(
    in1 => sig_in1,
    in2 => sig_in2,
    div => sig_div  ); 


  -- Sélection de sortie
  out_fx <= sig_sum & "0000000" when (sel_op = "00") else
            sig_mul             when (sel_op = "01") else
            sig_div             when (sel_op = "10") else
            (others=>'0');

end Behavioral;
