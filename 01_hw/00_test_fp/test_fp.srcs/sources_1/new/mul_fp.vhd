----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2020 11:31:30 AM
-- Design Name: 
-- Module Name: mul_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;


entity mul_fp is
  port ( in1, in2  :  in sfixed(4 downto -3);
         mul       : out sfixed(9 downto -6) );
end mul_fp;

architecture Behavioral of mul_fp is

signal sig_num1, sig_num2  : sfixed(4 downto -3);
signal sig_mul             : sfixed(9 downto -6);


begin

sig_num1 <= in1; -- Entrée 1
sig_num2 <= in2; -- Entrée 2

mul <= sig_mul;  -- Sortie

-- Faire la multiplication
sig_mul <=  sig_num1 * sig_num2;    -- mul


end Behavioral;