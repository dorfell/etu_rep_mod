----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31/05/2020 
-- Design Name: 
-- Module Name: add_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;


entity add_fp is
  port ( sum1, sum2, sum3 :  in sfixed(1 downto -13);
         sum4, sum5       :  in sfixed(1 downto -13);
         sum6             : out sfixed(2 downto -13) );
end add_fp;


architecture Behavioral of add_fp is

  signal sig_tmp1, sig_tmp2 : sfixed(2 downto -13);
  signal sig_tmp3           : sfixed(2 downto -13);

  signal sig_tmp1_t, sig_tmp2_t : sfixed(1 downto -13);
  signal sig_tmp3_t             : sfixed(1 downto -13);

begin

  -- Faire l'addition
  --sum6 <= sum1 + sum2 + sum3 + sum4 + sum5;    -- sum
  sig_tmp1   <= sum1 + sum2;
  sig_tmp1_t <= sig_tmp1(2) & sig_tmp1(0 downto -13);

  sig_tmp2   <= sig_tmp1_t + sum3;
  sig_tmp2_t <= sig_tmp2(2) & sig_tmp2(0 downto -13);

  sig_tmp3   <= sig_tmp2_t + sum4;
  sig_tmp3_t <= sig_tmp3(2) & sig_tmp3(0 downto -13);

  sum6   <= sig_tmp3_t + sum5;

end Behavioral;