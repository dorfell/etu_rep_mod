----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/24/2020 04:14:57 PM
-- Design Name: 
-- Module Name: mem_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;
use work.float_pkg.all;

entity mem_fp is
  port ( sel_in1  :  in std_logic;
         sel_in2  :  in std_logic; 
         A        : out sfixed(4 downto -3);
         B        : out sfixed(4 downto -3) );
end mem_fp;

architecture Behavioral of mem_fp is

signal sig_A_pos, sig_A_neg : sfixed(4 downto -3);
signal sig_B_pos, sig_B_neg : sfixed(4 downto -3);

begin

sig_A_pos <= to_sfixed ( 5.5,  sig_A_pos); -- n1  =  "00101110" = 5.7
sig_A_neg <= to_sfixed (-5.5,  sig_A_neg); -- n1  =  "00101110" = 5.7

sig_B_pos <= to_sfixed ( 6.25, sig_B_pos); -- n1  =  "00101110" = 5.7
sig_B_neg <= to_sfixed (-6.25, sig_B_neg); -- n1  =  "00101110" = 5.7

A <= sig_A_pos when (sel_in1 = '0') else sig_A_neg;
B <= sig_B_pos when (sel_in2 = '0') else sig_B_neg;


end Behavioral;
