----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31/05/2020 
-- Design Name: 
-- Module Name: mul_fp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;


entity mul_fp is
  port ( mul1, mul2  :  in sfixed(1 downto -6);
         mul3        : out sfixed(3 downto -12) );
end mul_fp;

architecture Behavioral of mul_fp is

begin

  -- Faire la multiplication
  mul3 <= mul1*mul2;    -- mul

end Behavioral;