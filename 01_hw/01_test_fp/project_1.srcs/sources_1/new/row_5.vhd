----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31/05/2020
-- Design Name: 
-- Module Name: row_5 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;

entity row_5 is
  port ( x1, x2, x3, x4, x5 :  in sfixed(1 downto  -6);
         k1, k2, k3, k4, k5 :  in sfixed(1 downto  -6);
         row5               : out sfixed(1 downto  -13) );
end row_5;

architecture Behavioral of row_5  is
  
    -- Fixed Point addition Component
  component add_fp
    port( sum1, sum2, sum3  :  in sfixed(1 downto -13);
          sum4, sum5        :  in sfixed(1 downto -13);
          sum6              : out sfixed(2 downto -13) );
  end component;

  -- Fixed Point multiplication Component
  component mul_fp
    port( mul1, mul2 :  in sfixed(1 downto  -6);
          mul3       : out sfixed(3 downto -12) );
  end component;
             
  
  signal sig_sum            : sfixed(2 downto -13);
  
  signal sig_mul1, sig_mul2 : sfixed(3 downto -12);
  signal sig_mul3, sig_mul4 : sfixed(3 downto -12);
  signal sig_mul5           : sfixed(3 downto -12);
  
  signal sig_mul1_t, sig_mul2_t : sfixed(1 downto -13);
  signal sig_mul3_t, sig_mul4_t : sfixed(1 downto -13);
  signal sig_mul5_t             : sfixed(1 downto -13);

    
begin

  ins_mul_1: mul_fp port map(
    mul1 => x1,
    mul2 => k1,
    mul3 => sig_mul1  ); 

  ins_mul_2: mul_fp port map(
    mul1 => x2,
    mul2 => k2,
    mul3 => sig_mul2  );
     
  ins_mul_3: mul_fp port map(
    mul1 => x3,
    mul2 => k3,
    mul3 => sig_mul3  );
     
  ins_mul_4: mul_fp port map(
    mul1 => x4,
    mul2 => k4,
    mul3 => sig_mul4  );
     
  ins_mul_5: mul_fp port map(
    mul1 => x5,
    mul2 => k5,
    mul3 => sig_mul5  ); 

  -- Q5,12 à Q2,13   
  sig_mul1_t <= sig_mul1(3) & sig_mul1(0) & sig_mul1(-1 downto -12) & '0';
  sig_mul2_t <= sig_mul2(3) & sig_mul2(0) & sig_mul2(-1 downto -12) & '0';
  sig_mul3_t <= sig_mul3(3) & sig_mul3(0) & sig_mul3(-1 downto -12) & '0';
  sig_mul4_t <= sig_mul4(3) & sig_mul4(0) & sig_mul4(-1 downto -12) & '0';
  sig_mul5_t <= sig_mul5(3) & sig_mul5(0) & sig_mul5(-1 downto -12) & '0';
  
  ins_add: add_fp port map(
    sum1 => sig_mul1_t,
    sum2 => sig_mul2_t,
    sum3 => sig_mul3_t,
    sum4 => sig_mul4_t,
    sum5 => sig_mul5_t,
    sum6 => sig_sum   );
    
  -- Q5,12 à Q2,13   
  row5 <= sig_sum(2) & sig_sum(0 downto -13);
     
end Behavioral;
