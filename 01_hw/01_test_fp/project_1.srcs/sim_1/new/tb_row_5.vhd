----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/01/2020 11:28:46 AM
-- Design Name: 
-- Module Name: tb_row_5 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.fixed_float_types.all;
use work.fixed_pkg.all;

entity tb_row_5 is
--  Port ( );
end tb_row_5;

architecture Behavioral of tb_row_5 is

  -- Component Declaration for the Unit Under Test (UUT)
  component row_5
    port ( x1, x2, x3, x4, x5 :  in sfixed(1 downto  -6);
           k1, k2, k3, k4, k5 :  in sfixed(1 downto  -6);
           row5               : out sfixed(1 downto  -13) );
  end component;
  
  --Inputs
  signal x1, x2, x3, x4, x5 : sfixed(1 downto -6) := (others => '0');
  signal k1, k2, k3, k4, k5 : sfixed(1 downto -6) := (others => '0');

  --Outputs
  signal row5 : sfixed(1 downto -13) := (others => '0');
   
begin

  -- Instantiate the Unit Under Test (UUT)
  uut: row_5 port map (
    x1   => x1, 
    x2   => x2,
    x3   => x3,
    x4   => x4,
    x5   => x5,
    k1   => k1,
    k2   => k2,
    k3   => k3,
    k4   => k4,
    k5   => k5,
    row5 => row5   );
  
  -- Stimulus process
  stim_proc: process
  begin		
    -- hold reset state for 100 ns.
    wait for 100 ns;	

    --wait for <clock>_period*10;

    -- insert stimulus here
    -- Q2,6 
    x1 <= b"00_000_001"; --  0.02 ~  0.015625
    x2 <= b"00_000_001"; --  0.02 ~  0.015625
    x3 <= b"00_000_001"; --  0.02 ~  0.015625
    x4 <= b"00_000_001"; --  0.02 ~  0.015625
    x5 <= b"00_000_001"; --  0.02 ~  0.015625

    k1 <= b"00_010_000"; --  0.25 
    k2 <= b"11_110_001"; -- -0.24 ~ -0.234375
    k3 <= b"11_101_011"; -- -0.34 ~ -0.328125
    k4 <= b"00_010_011"; --  0.30 ~  0.2969
    k5 <= b"11_110_000"; -- -0.25 ~ -0.25  
	wait for 100 ns;	

    -- Q2,6: Résultat: 1.25 => 01_010_000   
    x1 <= b"00_100_000"; --  0.50 ~  0.50
    x2 <= b"00_100_000"; --  0.50 ~  0.50
    x3 <= b"00_100_000"; --  0.50 ~  0.50
    x4 <= b"00_100_000"; --  0.50 ~  0.50
    x5 <= b"00_100_000"; --  0.50 ~  0.50

    k1 <= b"00_100_000"; --  0.50 ~  0.50 
    k2 <= b"00_100_000"; --  0.50 ~  0.50
    k3 <= b"00_100_000"; --  0.50 ~  0.50
    k4 <= b"00_100_000"; --  0.50 ~  0.50
    k5 <= b"00_100_000"; --  0.50 ~  0.50  
	wait for 100 ns;	

    -- Q2,6: Résultat: -0.3125 => 11_101_100   
    x1 <= b"00_010_000"; --  0.25 ~  0.25
    x2 <= b"00_010_000"; --  0.25 ~  0.25
    x3 <= b"00_010_000"; --  0.25 ~  0.25
    x4 <= b"00_010_000"; --  0.25 ~  0.25
    x5 <= b"00_010_000"; --  0.25 ~  0.25

    k1 <= b"11_110_000"; -- -0.25 ~ -0.25 
    k2 <= b"11_110_000"; -- -0.25 ~ -0.25
    k3 <= b"11_110_000"; -- -0.25 ~ -0.25
    k4 <= b"11_110_000"; -- -0.25 ~ -0.25
    k5 <= b"11_110_000"; -- -0.25 ~ -0.25  
	wait for 100 ns;	

    -- Q2,6: Résultat:  0.3125 => 11_010_100   
    x1 <= b"00_010_000"; --  0.25 ~  0.25
    x2 <= b"00_010_000"; --  0.25 ~  0.25
    x3 <= b"00_010_000"; --  0.25 ~  0.25
    x4 <= b"00_010_000"; --  0.25 ~  0.25
    x5 <= b"00_010_000"; --  0.25 ~  0.25

    k1 <= b"00_010_000"; --  0.25 ~  0.25
    k2 <= b"00_010_000"; --  0.25 ~  0.25
    k3 <= b"00_010_000"; --  0.25 ~  0.25
    k4 <= b"00_010_000"; --  0.25 ~  0.25
    k5 <= b"00_010_000"; --  0.25 ~  0.25
	wait for 100 ns;	
		
    wait;
  end process;

end Behavioral;
