-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/01_hw/ - Développement hardware d'un 
                          accelerateur pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Ce dépôt a des fichiers  qui sont utilisées pour le développement hardware 
d'un accelerateur FER.

00_test_fp
 -Projet en Vivado pour tester la bibliothèque ieee_proposed pour représentations
  de nombres en point fixé et en point flottant en VHDL. Dans ce project les
  operations d'addition, soustraction, multiplication et division sont testées. 

01_test_fp
 -Projet en Vivado pour faire la multiplication et addition de deux vecteurs du 
  5 élémentes avec nombres en représentation  du point fixé du 16 bits; Q3,13.

02_test_int
 -Projet en Vivado pour tester les operations avec nombres entiers en VHDL. Dans
  ce project les operations d'addition, soustraction, multiplication et 
  division sont simulées et implementées en Zybo-Z7. 

03_test_int
 -Projet en Vivado pour faire la multiplication entre deux vecteurs (i.e. x, w) de
  cinq elements en representation du nombres entiers. Le project a été testée en
  la carte Zybo-Z7.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
