
#LEDs
set_property -dict { PACKAGE_PIN D18   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[9] }]; # LED3
set_property -dict { PACKAGE_PIN G14   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[8] }]; # LED4

#Pmod Header JD
set_property -dict { PACKAGE_PIN T14   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[4] }]; # JD pin 1
set_property -dict { PACKAGE_PIN T15   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[5] }]; # JD pin 2
set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[6] }]; # JD pin 3
set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[7] }]; # JD pin 4

#Pmod Header JC
set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[0] }]; # JC pin 1
set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[1] }]; # JC pin 2
set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[2] }]; # JC pin 3
set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33 } [get_ports { out_vec_mul[3] }]; # JC pin 4

##Switches
set_property -dict { PACKAGE_PIN G15   IOSTANDARD LVCMOS33 } [get_ports { sel[0]   }]; # SW0
set_property -dict { PACKAGE_PIN P15   IOSTANDARD LVCMOS33 } [get_ports { sel[1]   }]; # SW1