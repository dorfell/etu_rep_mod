----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/11/2020 18:25:35 
-- Design Name: 
-- Module Name: tb_vec_mul - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- User types
use work.int_types.all;


entity tb_vec_mul is
--  Port ( );
end tb_vec_mul;

architecture Behavioral of tb_vec_mul is

  -- Component Declaration for the Unit Under Test (UUT)
  component top_vec_mul is
    port( sel         :   in std_logic_vector(1 downto 0);
          out_vec_mul : out integer range -255 to  256 );
  end component;

  -- Inputs
  signal sel : std_logic_vector(1 downto 0);
  
  -- Outputs
  signal out_vec_mul : integer range -255 to  256;

begin

    uut: top_vec_mul port map(
         sel => sel,
         out_vec_mul => out_vec_mul
         ); 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 5 ns;	
      --wait;
      
      --  5 et 10   
      sel  <= "00"; -- x= w =  vec_mul
      wait for 5 ns;  
      sel  <= "01"; -- x= w =  vec_mul
      wait for 5 ns;  
      sel  <= "10"; -- x= w =  vec_mul
      wait for 5 ns;  
      sel  <= "11"; -- x= w =  vec_mul
      wait for 5 ns;  

      wait; 

      
end process;		

end Behavioral;