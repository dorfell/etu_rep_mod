----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/11/2020 17:15:27 
-- Design Name: 
-- Module Name: add_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity add_int is
  port ( in0, in1  :  in integer range -127 to  128;
         in2, in3  :  in integer range -127 to  128;
         in4       :  in integer range -127 to  128;
         sum       : out integer range -255 to  256 );
end add_int;

architecture Behavioral of add_int is

begin

-- Faire l'addition
sum <=  in0 + in1 + in2 + in3 + in4;    

end Behavioral;