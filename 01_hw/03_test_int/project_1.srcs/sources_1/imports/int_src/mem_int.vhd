----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2020 22:02:57 
-- Design Name: 
-- Module Name: mem_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- User types
use work.int_types.all;


entity mem_int is
  port ( sel      :  in std_logic_vector(1 downto 0); 
         x        : out int_arr;
         w        : out int_arr );
end mem_int;

architecture Behavioral of mem_int is

signal x_arr, w_arr : int_arr;

begin

process(sel) is
begin 
  case sel is
    when "00" => -- x, w avec valeurs différentes
      x_arr(0) <= -10; x_arr(1) <=  10; x_arr(2) <=   0; x_arr(3) <=   5; x_arr(4) <=  -5;
      w_arr(0) <=   5; w_arr(1) <=  -5; w_arr(2) <=  10; w_arr(3) <= -10; w_arr(4) <=   0;
    when "01" => -- x, w avec valeurs de x
      x_arr(0) <= -10; x_arr(1) <=  10; x_arr(2) <=   0; x_arr(3) <=   5; x_arr(4) <=  -5;
      w_arr(0) <= -10; w_arr(1) <=  10; w_arr(2) <=   0; w_arr(3) <=   5; w_arr(4) <=  -5;
    when "10" => -- x, w avec valeurs de w
      x_arr(0) <=   5; x_arr(1) <=  -5; x_arr(2) <=  10; x_arr(3) <= -10; x_arr(4) <=   0;
      w_arr(0) <=   5; w_arr(1) <=  -5; w_arr(2) <=  10; w_arr(3) <= -10; w_arr(4) <=   0;
    when others =>
      x_arr(0) <=  -5; x_arr(1) <=  -5; x_arr(2) <=  -5; x_arr(3) <=  -5; x_arr(4) <=  -5;
      w_arr(0) <=  -5; w_arr(1) <=  -5; w_arr(2) <=  -5; w_arr(3) <=  -5; w_arr(4) <=  -5;
  end case;
end process;  
  
  -- sorties
  x <= x_arr; w <= w_arr;
end Behavioral;
