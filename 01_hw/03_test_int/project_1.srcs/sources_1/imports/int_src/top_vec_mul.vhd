----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/11/2020 19:30:30 
-- Design Name: 
-- Module Name: top_vec_mul - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- User types
use work.int_types.all;

entity top_vec_mul is
  port ( sel         :   in std_logic_vector(1 downto 0);
         out_vec_mul : out integer range -255 to  256  );
end top_vec_mul;

architecture Behavioral of top_vec_mul is

  -- Memory Component Declaration
  component mem_int
    port ( sel      :  in std_logic_vector(1 downto 0); 
           x        : out int_arr;
           w        : out int_arr );
  end component;
  
    -- Fixed Point addition Component
  component add_int
    port ( in0, in1  :  in integer range -127 to  128;
           in2, in3  :  in integer range -127 to  128;
           in4       :  in integer range -127 to  128;
           sum       : out integer range -255 to  256 );
  end component;

  -- Fixed Point multiplication Component
  component mul_int
    port( in0, in1  :  in integer range -127 to  128;
          mul       : out integer range -127 to  128 );
  end component;
              
  signal sig_x, sig_w : int_arr;
  signal sig_mul0, sig_mul1 : integer range -127 to  128;
  signal sig_mul2, sig_mul3 : integer range -127 to  128;
  signal sig_mul4           : integer range -127 to  128;
    
begin

  ins_mem: mem_int port map(
    sel => sel, 
    x   => sig_x,
    w   => sig_w ); 

  ins_mul_x0w0: mul_int port map(
    in0 => sig_x(0),
    in1 => sig_w(0),
    mul => sig_mul0 ); 

  ins_mul_x1w1: mul_int port map(
    in0 => sig_x(1),
    in1 => sig_w(1),
    mul => sig_mul1 ); 

  ins_mul_x2w2: mul_int port map(
    in0 => sig_x(2),
    in1 => sig_w(2),
    mul => sig_mul2 ); 

  ins_mul_x3w3: mul_int port map(
    in0 => sig_x(3),
    in1 => sig_w(3),
    mul => sig_mul3 ); 

  ins_mul_x4w4: mul_int port map(
    in0 => sig_x(4),
    in1 => sig_w(4),
    mul => sig_mul4 ); 

  ins_add: add_int port map(
    in0 => sig_mul0,
    in1 => sig_mul1,
    in2 => sig_mul2,
    in3 => sig_mul3,
    in4 => sig_mul4,
    sum => out_vec_mul ); 
  
end Behavioral;