----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/11/2020 17:16:30 
-- Design Name: 
-- Module Name: mul_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mul_int is
  port ( in0, in1  :  in integer range -127 to  128;
         mul       : out integer range -127 to  128 );
end mul_int;

architecture Behavioral of mul_int is

begin

-- Faire la multiplication
mul <= in0 * in1; 

end Behavioral;
