----------------------------------------------------------------------------------
-- Create Date: 07/11/2020 17:15:27 
-- Design Name: 
-- Module Name: int_types - Behavioral
-- Dorfell Parra
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package int_types is

  --! Type pour vecteur des entiers
  type int_arr is array(0 to 4) of integer range -127 to  128;

end package int_types;

package body int_types is


end package body int_types;
